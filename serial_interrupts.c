//=========================================================================== 
//
// Description: This file contains the UART0 and UART3 ISR
//
// Michael Puehringer
// Oct 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "msp430.h"
#include "serial.h"
#include "macros.h"
#include "command.h"
#include "iot.h"

// global variables
unsigned int uart0_rx_rd = ZERO;
volatile unsigned int uart0_rx_wr = ZERO;
volatile unsigned int uart0_tx_rd = ZERO;
volatile unsigned int uart0_tx_wr = ZERO;
volatile char uart0_rx[UART0_RX_LENGTH];
volatile char uart0_tx[UART0_TX_LENGTH];
volatile char uart0_has_reveived = FALSE;

extern volatile char ip_found;
extern volatile char ssid_found;
extern volatile char tcp_server_started;
extern volatile char ip[IP_STR_LENGTH];
extern volatile char ssid[SSID_STR_LENGTH];
extern volatile char tcp_connected[ONE];

// Pattern matching state machines
pattern_e ip_addr_pattern = { "Gateway   \r\n", (char*) ip, TWELVE, ZERO, ZERO, FIFTEEN, COLON, FALSE };
pattern_e ip_addr_pattern_alt = { "IP addr=", (char*) ip, EIGHT, ZERO, ZERO, FIFTEEN, SPACE, FALSE };
pattern_e ssid_pattern = { "SSID=\"", (char*) ssid, SIX, ZERO, ZERO, TEN, DOUBLE_QUOTE, FALSE };
pattern_e tcp_server_pattern = { "CONNECT ", (char*) tcp_connected, EIGHT, ZERO, ZERO, ONE, SPACE, FALSE };

// ringbuffer for receiving characters on UART3
volatile char uart3_rx[UART3_RX_LENGTH];
unsigned int uart3_rx_rd = ZERO;
volatile unsigned int uart3_rx_wr = ZERO;
// ringbuffer for transmitting characters on UART3
volatile char uart3_tx[UART3_TX_LENGTH];
volatile unsigned int uart3_tx_rd = ZERO;
volatile unsigned int uart3_tx_wr = ZERO;
// indicator if a command is currently received
volatile char uart3_is_receiving_cmd = FALSE;
// indicator if a command was fully received
volatile char uart3_has_cmd = FALSE;

//=========================================================================== 
// Description: ISR for UART0. Received characters are stored in the buffer and 
//              characters to transmit are transmitted. 
// 
// Passed: no variables passed
// Locals: 
//   temp: Temporary index store
// Returned: no variables returned
// Globals: 
//   uart0_rx
//   uart0_rx_wr
//   uart0_tx
//   uart0_tx_rd
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void){
  int temp;
  switch(__even_in_range(UCA0IV,ISR_SERIAL_RANGE_BOUND)){
  case ISR_SERIAL_CASE_NO_INTERRUPT: // Vector 0 - no interrupt
    break;
  case ISR_SERIAL_CASE_RXIFG: // Vector 2 - RXIFG
    uart0_has_reveived = TRUE;
    temp = uart0_rx_wr;
    uart0_rx[temp++] = UCA0RXBUF; // RX -> uart0_rx character
    temp %= UART0_RX_LENGTH;
    uart0_rx_wr = temp;
    uart_write_char(UART3, UCA0RXBUF); // Loop back to UART3
    break;
  case ISR_SERIAL_CASE_TXIFG: // Vector 4 � TXIFG    
    //if(!uart0_has_reveived) break;
    temp = uart0_tx_rd;
    UCA0TXBUF = uart0_tx[temp++];
    temp %= UART0_TX_LENGTH;
    if(temp == uart0_tx_wr) {
      UCA0IE &= ~UCTXIE; // Disable TX interrupt
    }
    uart0_tx_rd = temp;
    break;
  default: break;
  }
}

//=========================================================================== 
// Description: ISR for UART3. Received characters are stored in the buffer and 
//              characters to transmit are transmitted. 
// 
// Passed: no variables passed
// Locals: 
//   temp: Temporary index store
// Returned: no variables returned
// Globals: 
//   uart3_rx
//   uart3_rx_wr
//   uart3_tx
//   uart3_tx_rd
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
#pragma vector=USCI_A3_VECTOR
__interrupt void USCI_A3_ISR(void){
  int temp;
  switch(__even_in_range(UCA3IV,ISR_SERIAL_RANGE_BOUND)){
  case ISR_SERIAL_CASE_NO_INTERRUPT: // Vector 0 - no interrupt
    break;
  case ISR_SERIAL_CASE_RXIFG: // Vector 2 - RXIFG
    
    // if received character is the start of a command ..
    if(UCA3RXBUF == CMD_START) {
      uart3_is_receiving_cmd = TRUE;
    } 
    
    // if a command is being received ..
    if(uart3_is_receiving_cmd) {
      // .. add it to the receive buffer
      temp = uart3_rx_wr;
      uart3_rx[temp++] = UCA3RXBUF; // RX -> uart3_rx character
      temp %= UART3_RX_LENGTH;
      uart3_rx_wr = temp;
    }
    
    // if received character is the end of a command ..
    if(UCA3RXBUF == CMD_END && uart3_is_receiving_cmd) {
      uart3_is_receiving_cmd = FALSE;
      // indicate that a command is available
      uart3_has_cmd = TRUE;
    }
    
    // if no IP was found yet
    if(!ip_found) {
      // match the pattern of the start up message
      ip_found = match_pattern(&ip_addr_pattern, UCA3RXBUF);
      // if pattern was fully matched ..
      if(ip_found) {
        // execute the status command
        iot_network_status();
      }
    }
    
    // if no IP was found yet
    if(!ip_found) {
      // match the pattern with the network status message
      ip_found = match_pattern(&ip_addr_pattern_alt, UCA3RXBUF);
      // if IP was found and the TCP server is not yet started ..
      if(ip_found && !tcp_server_started) {
        // start the tcp server
        iot_start_tcp_server();
      }
    }
    
    // if no SSID was found yet ..
    if(!ssid_found) {
      // match the pattern with the network status message
      ssid_found = match_pattern(&ssid_pattern, UCA3RXBUF);
    }
    
    // if the TCP server is not yet started ..
    if(!tcp_server_started) {
      // match the pattern with the TCP server message
      tcp_server_started = match_pattern(&tcp_server_pattern, UCA3RXBUF);
    }
    
    // always loop back to UART0
    uart_write_char(UART0, UCA3RXBUF); 
    break;
  case ISR_SERIAL_CASE_TXIFG: // Vector 4 � TXIFG
    temp = uart3_tx_rd;
    UCA3TXBUF = uart3_tx[temp++];
    temp %= UART3_TX_LENGTH;
    if(temp == uart3_tx_wr) {
      UCA3IE &= ~UCTXIE; // Disable TX interrupt
    }
    uart3_tx_rd = temp;
    break;
  default: break;
  }
}

//=========================================================================== 
// Description: State machine for pattern finding
// 
// Passed: 
//   pattern_e* p  .. pointer to the pattern state machine
//   char rx       .. character to be processed
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
char match_pattern(pattern_e* p, char rx) {
  if(p->reading) { // if the pattern was matched, read the following n characters ..
    // check if the end character was received or the maximum amount of characters was reached ..
    if(rx == p->end_character || p->buffer_counter == p->end_length) {
      // .. then reading is done 
      p->reading = FALSE;
      // the result is saved
      p->buffer[p->buffer_counter] = STR_END;
      // and the counter is reset
      p->buffer_counter = ZERO;
      // return that we have the result
      return TRUE;
    } else {
      // otherwise, add the character to the result buffer
      p->buffer[p->buffer_counter++] = rx;
    }
  } else if(p->pattern_counter > ZERO) { // if the pattern is currently matching ..
    // check if the counter is within the length and the character matches ..
    if(p->pattern_counter < p->pattern_length && p->pattern[p->pattern_counter] == rx) {
      // .. then increase the counter
      p->pattern_counter++;
      // and check if the last character of the pattern was read ..
      if(p->pattern_counter == p->pattern_length) {
        // .. then enable reading on the next character
        p->reading = TRUE;
      }
    } else {
      // otherwise, the wrong character was received
      p->pattern_counter = ZERO;
    }
  } else if (p->pattern[p->pattern_counter] == rx) { // if the start character of the pattern was found ..
    p->pattern_counter++;
  }
  return FALSE;
}