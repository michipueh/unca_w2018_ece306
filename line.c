//=========================================================================== 
//
// Description: This file contains the implementation of the line following 
//              state machine
//
// Michael Puehringer
// Oct 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "line.h"
#include "functions.h"
#include "macros.h"
#include "msp430.h"
#include "motor.h"

unsigned int left_white = DEFAULT_LEFT_WHITE, 
              right_white = DEFAULT_RIGHT_WHITE, 
              left_black = DEFAULT_LEFT_BLACK, 
              right_black = DEFAULT_RIGHT_BLACK, 
              left_ambient = DEFAULT_LEFT_AMBIENT, 
              right_ambient = DEFAULT_RIGHT_AMBIENT;

circling_e circleState = TURNED_OFF;
circling_e lastDrift = LEFT_OFF_LINE;

// variables from adc_interrupts.c
extern volatile int ADC_Right_Detector;
extern volatile int ADC_Left_Detector;
// variables from timer_interrupts.c
extern volatile unsigned long time_in_s;

char line_detected = FALSE;
//int left_max = DEFAULT_LEFT_BLACK, 
//  right_max = DEFAULT_RIGHT_BLACK, 
//  left_min = DEFAULT_LEFT_WHITE, 
//  right_min = DEFAULT_RIGHT_WHITE;

//=========================================================================== 
// Description: Handling of the line following state machine. 
// 
// Passed : no variables passed 
// Locals: 
//  left_threshold
//  right_threshold
//  left
//  right 
// Returned: no values returned 
// Globals: 
//  left_white
//  right_white
//  left_black
//  right_black
//  left_ambient
//  right_ambient
//  circleState
//  line_detected
//  
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void process_line(void) {
  if(circleState != TURNED_OFF) {
    //if(left_max < ADC_Left_Detector) left_max = ADC_Left_Detector;
    //if(right_max < ADC_Right_Detector) right_max = ADC_Right_Detector;
    //if(left_min > ADC_Left_Detector) left_min = ADC_Left_Detector;
    //if(right_min > ADC_Right_Detector) right_min = ADC_Right_Detector;
    
    //left_threshold = (left_max - left_min) / TWO + left_min;
    //right_threshold = (right_max - right_min) / TWO + right_min;
    int left_threshold = (left_black - left_white) / TWO + left_white;
    int right_threshold = (right_black - right_white) / TWO + right_white;
    char left = (ADC_Left_Detector > left_threshold);
    char right = (ADC_Right_Detector > right_threshold);
    
    char left_w = (ADC_Left_Detector < left_white * TWO);
    char right_w = (ADC_Right_Detector < right_white * TWO);
    
    // left_min .. 0%
    // left_max .. 100%
    // left_min <= ADC_Left_Detector <= left_max
    // => 
    //float left_perc = ((float) ADC_Left_Detector - (float) left_min) / ((float) left_max - (float) left_min);
    //float right_perc = ((float) ADC_Right_Detector - (float) right_min) / ((float) right_max - (float) right_min);
    if(left_w && right_w && circleState == SEARCH_FOR_WHITE) {
      circleState = SEARCH_FOR_LINE;
    } 
    if(circleState != SEARCH_FOR_WHITE) {
      if(right && !left) {
        circleState = LEFT_OFF_LINE;
      } else if (left && !right) {
        circleState = RIGHT_OFF_LINE;
      } else if(!left && !right && circleState != SEARCH_FOR_LINE) {
        circleState = LOST_LINE;
      } else if(left && right) {
        if(!line_detected) {
          line_detected = TRUE;
          //both_motors_off();
          //delay_ms(THOUSAND);
        }
        circleState = ON_LINE;
      }
    }
    
    switch(circleState) {
    case SEARCH_FOR_WHITE: {
      left_motor_forward_speed(SPEED_MEDIUM);
      right_motor_forward_speed(SPEED_MEDIUM);
      break;
    }
    case SEARCH_FOR_LINE: {
      left_motor_forward_speed(SPEED_MEDIUM);
      right_motor_forward_speed(SPEED_MEDIUM);
      break;
    }
    case LOST_LINE: {
      if(lastDrift == RIGHT_OFF_LINE) {
        //left_motor_off();
        left_motor_reverse_speed(SPEED_SLOW);
        right_motor_forward_speed(SPEED_MEDIUM);
      } else {
        //right_motor_off();
        right_motor_reverse_speed(SPEED_SLOW);
        left_motor_forward_speed(SPEED_MEDIUM);
      }
      break;
    }
    case ON_LINE: {
      left_motor_forward_speed(SPEED_MEDIUM);
      right_motor_forward_speed(SPEED_MEDIUM);
      // TODO initBlack = TRUE;
      break;
    }
    case RIGHT_OFF_LINE: {
      //left_motor_off();
      left_motor_forward_speed(SPEED_SLOW);
      right_motor_forward_speed(SPEED_MEDIUM);
      lastDrift = RIGHT_OFF_LINE;
      break;
    }
    case LEFT_OFF_LINE: {
      //right_motor_off();
      right_motor_forward_speed(SPEED_SLOW);
      left_motor_forward_speed(SPEED_MEDIUM);
      lastDrift = LEFT_OFF_LINE;
      break;
    }
    }
  } else if(line_detected) {
    line_detected = FALSE;
    circleState = TURNED_OFF;
    both_motors_off();
  }
}