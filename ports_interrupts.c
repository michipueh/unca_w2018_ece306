//=========================================================================== 
//
// Description: This file contains the Ports ISRs
//
// Michael Puehringer
// Oct 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "msp430.h"
#include "macros.h"
#include "ports.h"

extern volatile unsigned int debounce_count_bt1;
extern volatile unsigned int debounce_count_bt2;
extern volatile char is_bt1_debouncing;
extern volatile char is_bt2_debouncing;

volatile char is_bt1_clicked = FALSE;
volatile char is_bt2_clicked = FALSE;

//=========================================================================== 
// Description: ISR for PORT5 buttons. Resets the debounce times for both 
//              buttons and sets the button pressed variables to TRUE. 
// 
// Passed : no variables passed 
// Locals: no variables declared
// Returned: no variables returned
// Globals: 
//   debounce_count_bt1
//   debounce_count_bt2
//   is_bt1_debouncing
//   is_bt2_debouncing
//   is_bt1_clicked
//   is_bt2_clicked
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
#pragma vector=PORT5_VECTOR
__interrupt void BUTTON_interrupt(void) {  
  // enable timer interrupt for debounce
  if(P5IFG & BUTTON1) {
    debounce_count_bt1 = ZERO;
    is_bt1_debouncing = TRUE;
    is_bt1_clicked = TRUE;
    TA0CCTL1 |= CCIE; // CCR1 enable interrupt
    P5IE &= ~BUTTON1; // disable interrupt
    P5IFG &= ~BUTTON1; // clear flag
  }
  
  if(P5IFG & BUTTON2) {
    debounce_count_bt2 = ZERO;
    is_bt2_debouncing = TRUE;
    is_bt2_clicked = TRUE;
    TA0CCTL1 |= CCIE; // CCR1 enable interrupt
    P5IE &= ~BUTTON2; // disable interrupt
    P5IFG &= ~BUTTON2; // clear flag
  }
}
