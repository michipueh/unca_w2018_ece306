//=========================================================================== 
//
// Description: This file contains the initialization and helper functions for 
//              UART0 and UART3
//
// Michael Puehringer
// Oct 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include <string.h>
#include "functions.h"
#include "msp430.h"
#include "serial.h"
#include "macros.h"

extern unsigned int uart0_rx_rd;
extern volatile unsigned int uart0_rx_wr;
extern volatile unsigned int uart0_tx_rd;
extern volatile unsigned int uart0_tx_wr;
extern volatile char uart0_rx[UART0_RX_LENGTH];
extern volatile char uart0_tx[UART0_TX_LENGTH];
extern unsigned int uart3_rx_rd;
extern volatile unsigned int uart3_rx_wr;
extern volatile unsigned int uart3_tx_rd;
extern volatile unsigned int uart3_tx_wr;
extern volatile char uart3_rx[UART3_RX_LENGTH];
extern volatile char uart3_tx[UART3_TX_LENGTH];
baud_rate_e baud_rate_UART0;
baud_rate_e baud_rate_UART3;

//=========================================================================== 
// Description: Calls Init_Serial_UCA0(RATE_115200) and Init_Serial_UCA3(RATE_115200). 
// 
// Passed : no variables passed 
// Locals: @see #Init_Serial_UCA0 and #Init_Serial_UCA3
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void Init_Serial(void) {
  Init_Serial_UCA0(RATE_115200);
  Init_Serial_UCA3(RATE_115200);
}

//=========================================================================== 
// Description: Initializes UART0 with a given baud rate. 
// 
// Passed : 
//   baud_rate_e baud_rate: Baud rate which should be set
// Locals: @see #uart_set_baudrate
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void Init_Serial_UCA0(const baud_rate_e baud_rate) {
  UCA0CTL1 |= UCSWRST; // Reset
  UCA0CTLW0 = ZERO; // Use word register
  UCA0CTLW0 |= UCSSEL__SMCLK; // Set SMCLK as f_BRCLK
  UCA0CTLW0 |= UCSWRST; // Set software reset enable
  // no parity, 8-bits, 1 stop bit
  uart_set_baudrate(UART0, baud_rate);
  UCA0CTL1 &= ~UCSWRST; // Release from reset
}

//=========================================================================== 
// Description: Initializes UART3 with a given baud rate. 
// 
// Passed : 
//   baud_rate_e baud_rate: Baud rate which should be set
// Locals: @see #uart_set_baudrate
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void Init_Serial_UCA3(const baud_rate_e baud_rate) {
  UCA3CTL1 |= UCSWRST; // Reset
  UCA3CTLW0 = ZERO; // Use word register
  UCA3CTLW0 |= UCSSEL__SMCLK; // Set SMCLK as f_BRCLK
  UCA3CTLW0 |= UCSWRST; // Set software reset enable
  
  uart_set_baudrate(UART3, baud_rate);
  UCA3CTL1 &= ~UCSWRST; // Release from reset
}

//=========================================================================== 
// Description: Sets the baud rate of a given UART. 
// 
// Passed : 
//   uart_channel_e channel: UART to be updated
//   baud_rate_e baud_rate: Baud rate which should be set
// Locals: 
//   char was_receiving: indicator if receive interrupt is already turned on. 
// Returned: no variables returned
// Globals: 
//   baud_rate_e baud_rate_UART0
//   baud_rate_e baud_rate_UART3
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void uart_set_baudrate(const uart_channel_e channel, const baud_rate_e baud_rate) {
  char was_receiving = uart_is_receiving(channel);
  if(was_receiving) {
    uart_stop_receiving(channel);
  }
  
  if(channel == UART0) {
    baud_rate_UART0 = baud_rate;
    UCA0CTL1 |= UCSWRST; // Reset
    if(baud_rate == RATE_115200) {
      UCA0BRW = BRW_115200;
      UCA0MCTLW = MCTLW_115200;
    } else if(baud_rate == RATE_460800) {
      UCA0BRW = BRW_460800; 
      UCA0MCTLW = MCTLW_460800;
    } else if(baud_rate == RATE_9600) {
      UCA0BRW = BRW_9600; // 9,600 Baud
      // UCA0MCTLW = UCSx concatenate UCFx concatenate UCOS16;
      // UCA0MCTLW = 0x49 concatenate 1 concatenate 1;
      UCA0MCTLW = MCTLW_9600;
    }
    UCA0CTL1 &= ~UCSWRST; // Release from reset
  } else if(channel == UART3) {
    baud_rate_UART3 = baud_rate;
    UCA3CTL1 |= UCSWRST; // Reset
    if(baud_rate == RATE_115200) {
      UCA3BRW = BRW_115200;
      UCA3MCTLW = MCTLW_115200;
    } else if(baud_rate == RATE_460800) {
      UCA3BRW = BRW_460800; 
      UCA3MCTLW = MCTLW_460800;
    } else if(baud_rate == RATE_9600) {
      UCA3BRW = BRW_9600; // 9,600 Baud
      // UCA0MCTLW = UCSx concatenate UCFx concatenate UCOS16;
      // UCA0MCTLW = 0x49 concatenate 1 concatenate 1;
      UCA3MCTLW = MCTLW_9600;
    }
    UCA3CTL1 &= ~UCSWRST; // Release from reset
  }
  
  if(was_receiving) {
    uart_start_receiving(channel);
  }
}

//=========================================================================== 
// Description: Starts receiving at a given UART. 
// 
// Passed : 
//   uart_channel_e channel: UART to be updated
// Locals: no variables declared 
// Returned: no variables returned
// Globals: no variables changed 
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void uart_start_receiving(const uart_channel_e channel) {
  if(channel == UART0) {
    UCA0IE |= UCRXIE; 
  } else if(channel == UART3) {
    UCA3IE |= UCRXIE; 
  }
} 

//=========================================================================== 
// Description: Stops receiving at a given UART. 
// 
// Passed : 
//   uart_channel_e channel: UART to be updated
// Locals: no variables declared 
// Returned: no variables returned
// Globals: no variables changed 
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void uart_stop_receiving(const uart_channel_e channel) {
  if(channel == UART0) {
    UCA0IE &= ~UCRXIE; 
  } else if(channel == UART3) {
    UCA3IE &= ~UCRXIE; 
  }
}

//=========================================================================== 
// Description: Checks if given UART is receiving. 
// 
// Passed : 
//   uart_channel_e channel: UART to be checked
// Locals: no variables declared 
// Returned: 
//   int: 1 if receiving, 0 otherwise
// Globals: no variables changed 
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
int uart_is_receiving(const uart_channel_e channel) {
  if(channel == UART0) {
    return UCA0IE & UCRXIE; 
  } else if(channel == UART3) {
    return UCA3IE & UCRXIE;
  }
  return ZERO;
}

//=========================================================================== 
// Description: Writes a char to the transmit buffer of a given UART. 
// 
// Passed : 
//   channel: UART where the char should be sent
//   tx: Character to be sent
// Locals: no variables declared 
// Returned: no variables returned
// Globals: 
//   uart0_tx
//   uart0_tx_wr
//   uart3_tx
//   uart3_tx_wr
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void uart_write_char(const uart_channel_e channel, char tx) {
  if(channel == UART0) {
    uart0_tx[uart0_tx_wr++] = tx; 
    uart0_tx_wr %= UART0_TX_LENGTH;
    UCA0IE |= UCTXIE; 
  } else if(channel == UART3) {
    uart3_tx[uart3_tx_wr++] = tx; 
    uart3_tx_wr %= UART3_TX_LENGTH;
    UCA3IE |= UCTXIE; 
  }
}

//=========================================================================== 
// Description: Writes a char array to the transmit buffer of a given UART and appends CR to it. 
// 
// Passed: 
//   channel: UART where the char should be sent
//   tx: Character array to be sent
// Locals: @see uart_write_char 
// Returned: no variables returned
// Globals: @see uart_write_char
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void uart_write_string(const uart_channel_e channel, const char* tx) {
  for (int i = ZERO; i < strlen(tx); i++){
    uart_write_char(channel, tx[i]);
  }
  uart_write_char(channel, CR);
}

//=========================================================================== 
// Description: Reads a char from the receive buffer of a given UART. 
// 
// Passed: 
//   channel: UART from where the char should be received
//   rx: Pointer to char where the result should be stored
//   blocking: Indicator if the function call should be blocking
// Locals: no variables declared
// Returned: 
//   char: 0 if nothing was received, 1 otherwise
// Globals: 
//   uart0_rx_rd
//   uart0_rx_wr
//   uart0_rx
//   uart3_rx_rd
//   uart3_rx_wr
//   uart3_rx
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
char uart_read_char(const uart_channel_e channel, char* rx, char blocking) {
  if(channel == UART0) {
    if(blocking) {
      while(uart0_rx_rd == uart0_rx_wr) {
        continue;
      }
    } else if(uart0_rx_rd == uart0_rx_wr) {
      return FALSE;
    }
    *rx = uart0_rx[uart0_rx_rd++];
    uart0_rx_rd %= UART0_RX_LENGTH;
  } else if (channel == UART3) {
    if(blocking) {
      while(uart3_rx_rd == uart3_rx_wr) {
        continue;
      }
    } else if(uart3_rx_rd == uart3_rx_wr) {
      return FALSE;
    }
    *rx = uart3_rx[uart3_rx_rd++];
    uart3_rx_rd %= UART3_RX_LENGTH;
  }
  return TRUE;
}

//=========================================================================== 
// Description: Reads a string (till \r or length) from the receive buffer of a given UART. 
// 
// Passed: 
//   channel: UART from where the char should be received
//   buffer: Character buffer where the result should be stored
//   length: Maximum length of characters to be read
//   blocking: Indicator if the function call should be blocking
// Locals: 
//   c: Temporary character store
//   i: Index counter of received characters
// Returned: 
//   char: 0 if nothing was received, 1 otherwise
// Globals: No variables changed
//
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
char uart_read_string(const uart_channel_e channel, char* buffer, unsigned int length, char blocking) {
  char c;
  int i = ZERO;
  while(uart_read_char(channel, &c, blocking) && c != CR && i < length) {
    buffer[i] = c;
    i++;
  }
  buffer[i] = STR_END;
  return i != ZERO;
}