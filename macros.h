// CPU Clock Times
#define CPU_CLK                 (8000000)
#define TEN_MS_CYCLES           (80000)
#define ONE_MS_CYCLES           (8000)

// General
#define ALWAYS                  (1)
#define RESET_STATE             (0)
#define PORTS                   (0x00) // PORTS
#define PWM_MODE                (0x01) // PWM_MODE

// Booleans
#define TRUE                    (1)
#define FALSE                   (0)

// Numbers
#define ZERO                    (0)
#define ONE                     (1)
#define TWO                     (2)
#define FIVE                    (5)
#define SIX                     (6)
#define EIGHT                   (8)
#define NINE                    (9)
#define TEN                     (10)
#define TWELVE                  (12)
#define FIFTEEN                 (15)
#define TWENTY                  (20)
#define HUNDRED                 (100)
#define TWO_HUNDRED             (200)
#define THREE_HUNDRED           (300)
#define FIVE_HUNDRED            (500)
#define EIGHT_HUNDRED           (800)
#define THOUSAND                (1000)
#define TWO_THOUSAND            (2000)
#define THREE_THOUSAND          (3000)
#define FIVE_THOUSAND           (5000)

// Characters
#define ZERO_CHAR               '0'
#define NINE_CHAR               '9'
#define COLON                   ':'
#define DOUBLE_QUOTE            '"'
#define SPACE                   ' '
#define CR                      '\r'
#define STR_END                 '\0'

// LCD
#define DISPLAY_LINES           (4)
#define DISPLAY_LINE_LENGTH     (11)
#define DISPLAY_LINE_1          (0)
#define DISPLAY_LINE_2          (1)
#define DISPLAY_LINE_3          (2)
#define DISPLAY_LINE_4          (3)
