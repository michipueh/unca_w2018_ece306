//=========================================================================== 
//
//  Description: This file contains the ADC ISR implementation
//
//  Michael Puehringer
//  Oct 2018
//  Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include  "msp430.h"
#include  "functions.h"
#include  "macros.h"
#include  "adc_interrupts.h"

// ADC value for thumbwheel
volatile int ADC_Thumb = ZERO;
// ADC value for right detector
volatile int ADC_Right_Detector = ZERO;
// ADC value for left detector
volatile int ADC_Left_Detector = ZERO;
// Current index in the right detector adc history array
volatile char adc_right_index = ZERO;
// History array for right detector adc (for averaging)
volatile int adc_right_arr[ADC_RIGHT_ARR_LENGTH];
// Current index in the left detector adc history array
volatile char adc_left_index = ZERO;
// History array for left detector adc (for averaging)
volatile int adc_left_arr[ADC_LEFT_ARR_LENGTH];

//=========================================================================== 
// Description: Calculates the average of an array with ignoring the minimum and maximum value. 
// 
// Passed :  
//   int array[] ... the values to calculate the average from
//   int length  ... the amount of values to be used for the calcuation
// Locals: 
//   int min     ... minimum value found in the array
//   int max     ... maximum value found in the array
// Returned:     ... int result of the avarge calculation
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
int calculateAverage(volatile int array[], volatile int length) {
  // reset result
  long sum = ZERO;
  // set min and max to first array value
  int min = array[ZERO];
  int max = array[ZERO];
  // for every value in the array
  for(int i = ZERO; i < length; i++) {
    // get the value
    int cur = array[i];
    // update min and max
    if(cur > max) max = cur;
    if(cur < min) min = cur;
    // and add it to the sum
    sum += cur;
  }
  // remove min and max value from the sum
  sum -= min;
  sum -= max;
  // return the sum divided by the length of the array (excluding min and max)
  return sum / (length - TWO);  
}

//=========================================================================== 
// Description: ISR for ADC for setting the current ADC values after they 
//              have been averaged using the last five values. 
// 
// Passed : no variables passed 
// Locals: 
//  temp
// Returned: no variables returned
// Globals: 
//  ADC_Thumb 
//  ADC_Right_Detector 
//  ADC_Left_Detector 
//  adc_right_index
//  adc_right_arr[]
//  adc_left_index
//  adc_left_arr[]
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
#pragma vector=ADC12_B_VECTOR
__interrupt void ADC12_ISR(void) {
  int temp;
  switch(__even_in_range(ADC12IV, ADC12IV__ADC12RDYIFG)) 
  {
  case ADC12IV__NONE: break; // Vector 0: No interrupt
  case ADC12IV__ADC12OVIFG: break; // Vector 2: ADC12MEMx Overflow
  case ADC12IV__ADC12TOVIFG: break; // Vector 4: Conversion time overflow
  case ADC12IV__ADC12HIIFG: break; // Vector 6: ADC12BHI
  case ADC12IV__ADC12LOIFG: break; // Vector 8: ADC12BLO
  case ADC12IV__ADC12INIFG: break; // Vector 10: ADC12BIN
  case ADC12IV__ADC12IFG0: break; // Vector 12: ADC12MEM0 Interrupt
  case ADC12IV__ADC12IFG1: break; // Vector 14: ADC12MEM1 Interrupt
  case ADC12IV__ADC12IFG2: // Vector 16: ADC12MEM2 Interrupt
    // assign adc value of the thumbwheel
    ADC_Thumb = ADC12MEM0; // A02 ADC10INCH_2
    
    // add the left detector adc value to the array
    temp = adc_left_index;
    adc_left_arr[temp] = ADC12MEM2;
    temp++;
    // and if the array is full ..
    if(temp == ADC_LEFT_ARR_LENGTH) {
      // .. calculate the average and assign the value
      ADC_Left_Detector = calculateAverage(adc_left_arr, ADC_LEFT_ARR_LENGTH);
      temp = ZERO;
    }
    // update the index
    adc_left_index = temp;
    
    // add the right detector adc value to the array
    temp = adc_right_index;
    adc_right_arr[temp] = ADC12MEM1;
    temp++;
    // and if the array is full ..
    if(temp == ADC_RIGHT_ARR_LENGTH) {
      // .. calculate the average and assign the value
      ADC_Right_Detector = calculateAverage(adc_right_arr, ADC_RIGHT_ARR_LENGTH);
      temp = ZERO;
    }
    // update the index
    adc_right_index = temp;
    break;
  case ADC12IV__ADC12IFG3: break; // Vector 18: ADC12MEM3
  case ADC12IV__ADC12IFG4: break; // Vector 20: ADC12MEM4
  case ADC12IV__ADC12IFG5: break; // Vector 22: ADC12MEM5
  case ADC12IV__ADC12IFG6: break; // Vector 24: ADC12MEM6
  case ADC12IV__ADC12IFG7: break; // Vector 26: ADC12MEM7
  case ADC12IV__ADC12IFG8: break; // Vector 28: ADC12MEM8
  case ADC12IV__ADC12IFG9: break; // Vector 30: ADC12MEM9
  case ADC12IV__ADC12IFG10: break; // Vector 32: ADC12MEM10
  case ADC12IV__ADC12IFG11: break; // Vector 34: ADC12MEM11
  case ADC12IV__ADC12IFG12: break; // Vector 36: ADC12MEM12
  case ADC12IV__ADC12IFG13: break; // Vector 38: ADC12MEM13
  case ADC12IV__ADC12IFG14: break; // Vector 40: ADC12MEM14
  case ADC12IV__ADC12IFG15: break; // Vector 42: ADC12MEM15
  case ADC12IV__ADC12IFG16: break; // Vector 44: ADC12MEM16
  case ADC12IV__ADC12IFG17: break; // Vector 46: ADC12MEM17
  case ADC12IV__ADC12IFG18: break; // Vector 48: ADC12MEM18
  case ADC12IV__ADC12IFG19: break; // Vector 50: ADC12MEM19
  case ADC12IV__ADC12IFG20: break; // Vector 52: ADC12MEM20
  case ADC12IV__ADC12IFG21: break; // Vector 54: ADC12MEM21
  case ADC12IV__ADC12IFG22: break; // Vector 56: ADC12MEM22
  case ADC12IV__ADC12IFG23: break; // Vector 58: ADC12MEM23
  case ADC12IV__ADC12IFG24: break; // Vector 60: ADC12MEM24
  case ADC12IV__ADC12IFG25: break; // Vector 62: ADC12MEM25
  case ADC12IV__ADC12IFG26: break; // Vector 64: ADC12MEM26
  case ADC12IV__ADC12IFG27: break; // Vector 66: ADC12MEM27
  case ADC12IV__ADC12IFG28: break; // Vector 68: ADC12MEM28
  case ADC12IV__ADC12IFG29: break; // Vector 70: ADC12MEM29
  case ADC12IV__ADC12IFG30: break; // Vector 72: ADC12MEM30
  case ADC12IV__ADC12IFG31: break; // Vector 74: ADC12MEM31
  case ADC12IV__ADC12RDYIFG: break; // Vector 76: ADC12RDY
  default: break;
  }
}
