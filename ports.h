void Init_Ports(void);
void Init_Port1(void);
void Init_Port2(void);
void Init_Port3(char port_type);
void Init_Port4(void);
void Init_Port5(void);
void Init_Port6(void);
void Init_Port7(void);
void Init_Port8(void);
void Init_PortJ(void);


// Pin Positions
#define PIN_0 (1 << 0)
#define PIN_1 (1 << 1)
#define PIN_2 (1 << 2)
#define PIN_3 (1 << 3)
#define PIN_4 (1 << 4)
#define PIN_5 (1 << 5)
#define PIN_6 (1 << 6)
#define PIN_7 (1 << 7)

#define PORT_1_DIR_DEFAULT 0x00;
#define PORT_2_DIR_DEFAULT 0x00;
#define PORT_3_DIR_DEFAULT 0x00;
#define PORT_4_DIR_DEFAULT 0x00;
#define PORT_5_DIR_DEFAULT 0x00;
#define PORT_6_DIR_DEFAULT 0x00;
#define PORT_7_DIR_DEFAULT 0x00;
#define PORT_8_DIR_DEFAULT 0x00;
#define PORT_J_DIR_DEFAULT 0x00;

#define SEL0_GPIO_DEFAULT 0x00;
#define SEL1_GPIO_DEFAULT 0x00;

// Port 1 Pins
#define RED_LED (PIN_0) // RED LED � P1_0_LED1
#define GRN_LED (PIN_1) // GREEN LED � P1_1_LED2
#define V_THUMB (PIN_2) // ADC for Thumb Wheel
#define TEST_PROBE (PIN_3) // GP I/O Pin to use to check code operation
#define V_DETECT_R (PIN_4) // ADC for Right Detector
#define V_DETECT_L (PIN_5) // ADC for Left Detector
#define SD_UCB0SIMO (PIN_6) // SPI mode - slave in/master out for SD Card
#define SD_UCB0SOMI (PIN_7) // SPI mode - slave out/master in for SD Card

// Port 2 Pins
#define UCA0TXD (PIN_0) // 
#define UCA0RXD (PIN_1) // 
#define SD_SPICLK (PIN_2) // 
#define P2_3 (PIN_3) //
#define P2_4 (PIN_4) //
#define UCA1TXD (PIN_5) // 
#define UCA1RXD (PIN_6) //
#define P2_7 (PIN_7) //

// Port 3 Pins
#define IOT_RESET (PIN_0) // 
#define IOT_PROG_MODE (PIN_1) // 
#define IOT_LINK (PIN_2) // 
#define IOT_PROG_SEL (PIN_3) // 
#define L_REVERSE (PIN_4) // 
#define SMCLK_OUT (PIN_4) // 
#define L_FORWARD (PIN_5) // 
#define R_REVERSE (PIN_6) // 
#define R_FORWARD (PIN_7) // 

#define USE_L_REVERSE (0x00) //
#define USE_SMCLK     (0x01) //

// Port 4 Pins
#define SD_CS (PIN_0) // 
#define J4_31 (PIN_1) // 
#define J4_32 (PIN_2) // 
#define J4_33 (PIN_3) // 
#define UCB1_CS_LCD (PIN_4) // 
#define P4_4 (PIN_5) // 
#define P4_5 (PIN_6) // 
#define J3_29 (PIN_7) // 

// Port 5 Pins
#define UCB1SIMO (PIN_0) // 
#define UCB1SOMI (PIN_1) // 
#define UCB1SCK (PIN_2) // 
#define RESET_LCD (PIN_3) // 
#define P5_4 (PIN_4) // 
#define BUTTON2 (PIN_5) // 
#define BUTTON1 (PIN_6) // 
#define LCD_BACKLITE (PIN_7) // 

// Port 6 Pins
#define UCA3TXD (PIN_0) // 
#define UCA3RXD (PIN_1) // 
#define J1_5 (PIN_2) // 
#define MAG_INT (PIN_3) // 
#define P6_4 (PIN_4) // 
#define P6_5 (PIN_5) // 
#define P6_6 (PIN_6) // 
#define P6_7 (PIN_7) // 

// Port 7 Pins
#define I2CSDA (PIN_0) // 
#define I2CSCL (PIN_1) // 
#define SD_DETECT (PIN_2) // 
#define J4_36 (PIN_3) // 
#define P7_4 (PIN_4) // 
#define P7_5 (PIN_5) // 
#define P7_6 (PIN_6) // 
#define P7_7 (PIN_7) // 

// Port 8 Pins
#define IR_LED (PIN_0) // 
#define OPT_INT (PIN_1) // 
#define TMP_INT (PIN_2) // 
#define INT2 (PIN_3) // 

// Port J Pins
#define PJ_0 (PIN_0) // 
#define PJ_1 (PIN_1) // 
#define PJ_2 (PIN_2) // 
#define PJ_3 (PIN_3) // 
#define LFXIN (PIN_4) // 
#define LFXOUT (PIN_5) // 
#define HFXIN (PIN_6) // 
#define HFXOUT (PIN_7) // 