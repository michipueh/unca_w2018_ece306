// PWM Registers
#define LEFT_REVERSE_SPEED TB0CCR3
#define LEFT_FORWARD_SPEED TB0CCR4
#define RIGHT_REVERSE_SPEED TB0CCR5
#define RIGHT_FORWARD_SPEED TB0CCR6

// PWM Configuration
#define WHEEL_PERIOD            (4000)
#define WHEEL_OFF               (0)
#define WHEEL_ON                (4000)
#define RIGHT_WHEEL_ON          (WHEEL_ON * 0.95)
#define LEFT_WHEEL_ON           (WHEEL_ON)
#define SPEED_SLOW              (0.40)
#define SPEED_MEDIUM            (0.5)
#define SPEED_FAST              (0.6)

void left_motor_off(void);
void left_motor_reverse_off(void); 
void left_motor_forward_off(void);
void left_motor_forward(void);
void left_motor_reverse(void);
void left_motor_forward_speed(float speed);
void left_motor_reverse_speed(float speed);

void right_motor_off(void);
void right_motor_reverse_off(void); 
void right_motor_forward_off(void);
void right_motor_forward(void);
void right_motor_reverse(void);
void right_motor_forward_speed(float speed);
void right_motor_reverse_speed(float speed);

void both_motors_off(void);
void both_motor_reverse_off(void); 
void both_motor_forward_off(void);
void both_motors_forward(void);
void both_motors_forward_speed(float speed);
void both_motors_reverse(void);
void both_motors_reverse_speed(float speed);