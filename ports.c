//=========================================================================== 
// 
// Description: This file contains the initialization for all port pins 
// 
// Author: Michael Puehringer 
// Date: September 2018 
// Compiler: Built with IAR Embedded Workbench Version: V7.12.1
//===========================================================================

#include "macros.h"
#include "functions.h"
#include "msp430.h"
#include "ports.h"

//=========================================================================== 
// Description: Calls all port-initialization functions. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Ports(void) {
  Init_Port1();
  Init_Port2();
  Init_Port3(USE_L_REVERSE); 
  Init_Port4();
  Init_Port5();
  Init_Port6();
  Init_Port7();
  Init_Port8();
  Init_PortJ();
}

//=========================================================================== 
// Description: Initializes all pins on port 1. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port1(void){
  //Configure Port 1
  P1SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P1SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P1DIR = PORT_1_DIR_DEFAULT; // Set P1 direction to input
  // P1_0
  P1OUT |= RED_LED; // Set Red LED On
  P1DIR |= RED_LED; // Set Red LED direction to output
  // P1_1
  P1OUT |= GRN_LED; // Set Green LED On
  P1DIR |= GRN_LED; // Set Green LED direction to output
  // P1_2
  P1SEL0 |= V_THUMB; // ADC input for Thumbwheel
  P1SEL1 |= V_THUMB; // ADC input for Thumbwheel
  // P1_3
  P1OUT &= ~TEST_PROBE; // Set TEST_PROBE Off
  P1DIR |= TEST_PROBE; // Set TEST_PROBE direction to output
  // P1_4
  P1SEL0 |= V_DETECT_R; // ADC input for Right Detector
  P1SEL1 |= V_DETECT_R; // ADC input for Right Detector
  // P1_5
  P1SEL0 |= V_DETECT_L; // ADC input for Left Detector
  P1SEL1 |= V_DETECT_L; // ADC input for Left Detector
  // P1_6
  P1SEL0 &= ~SD_UCB0SIMO; // USCI_B1 MOSI pin
  P1SEL1 |= SD_UCB0SIMO; // USCI_B1 MOSI pin
  // P1_7
  P1SEL0 &= ~SD_UCB0SOMI; // USCI_B1 MISO pin
  P1SEL1 |= SD_UCB0SOMI; // USCI_B1 MISO pin
}

//=========================================================================== 
// Description: Initializes all pins on port 2. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port2(void) {
  //Configure Port 2
  P2SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P2SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P2DIR = PORT_2_DIR_DEFAULT; // Set P1 direction to input
  // P2_0
  P2SEL0 &= ~UCA0TXD; // 
  P2SEL1 |= UCA0TXD; // 
  // P2_1
  P2SEL0 &= ~UCA0RXD; // 
  P2SEL1 |= UCA0RXD; // 
  // P2_2
  P2SEL0 &= ~SD_SPICLK; // 
  P2SEL1 |= SD_SPICLK; // 
  // P2_3
  P2SEL0 &= ~P2_3; // 
  P2SEL1 &= ~P2_3; // 
  // P2_4
  P2SEL0 &= ~P2_4; // 
  P2SEL1 &= ~P2_4; // 
  // P2_5
  P2SEL0 &= ~UCA1TXD; // 
  P2SEL1 |= UCA1TXD; // 
  // P2_6
  P2SEL0 &= ~UCA1RXD; // 
  P2SEL1 |= UCA1RXD; // 
  // P2_7
  P2SEL0 &= ~P2_7; // 
  P2SEL1 &= ~P2_7; // 
}

//=========================================================================== 
// Description: Initializes all pins on port 3. 
// 
// Passed : 
//   char mode .. mode of pin 4
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port3(char mode) {
  //Configure Port 3
  P3SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P3SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P3DIR = PORT_3_DIR_DEFAULT; // Set P1 direction to input
  // P3_0
  P3OUT &= ~IOT_RESET; // 
  P3DIR |= IOT_RESET; //
  // P3_1
  P3OUT &= ~IOT_PROG_MODE; // 
  P3DIR &= ~IOT_PROG_MODE; //
  // P3_2
  P3DIR &= ~IOT_LINK; //
  // P3_3
  P3OUT &= ~IOT_PROG_SEL; // 
  P3DIR &= ~IOT_PROG_SEL; //
  // P3_4
  if(mode == USE_L_REVERSE) {
    //P3OUT &= ~L_REVERSE; // 
    P3SEL0 |= L_REVERSE;
    P3SEL1 &= ~L_REVERSE;
    P3DIR |= L_REVERSE; //
  } else if (mode == USE_SMCLK) {
    P3SEL0 &= ~SMCLK_OUT; //
    P3SEL1 |= SMCLK_OUT; //
    P3DIR |= SMCLK_OUT; // 
  }
  // P3_5
  //P3OUT &= ~L_FORWARD; // 
  P3SEL0 |= L_FORWARD;
  P3SEL1 &= ~L_FORWARD;
  P3DIR |= L_FORWARD; //
  // P3_6
  //P3OUT &= ~R_REVERSE; // 
  P3SEL0 |= R_REVERSE;
  P3SEL1 &= ~R_REVERSE;
  P3DIR |= R_REVERSE; //
  // P3_7
  //P3OUT &= ~R_FORWARD; // 
  P3SEL0 |= R_FORWARD;
  P3SEL1 &= ~R_FORWARD;
  P3DIR |= R_FORWARD; //
}

//=========================================================================== 
// Description: Initializes all pins on port 4. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port4(void) {
  //Configure Port 4
  P4SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P4SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P4DIR = PORT_4_DIR_DEFAULT; // Set P1 direction to input
  
  // P4_0
  P4DIR |= SD_CS; //
  // P4_1
  P4DIR |= J4_31; //
  // P4_2
  P4DIR |= J4_32; //
  // P4_3
  P4DIR |= J4_33; //
  // P4_4
  P4OUT |= UCB1_CS_LCD; // 
  P4DIR |= UCB1_CS_LCD; //
  // P4_5
  P4DIR |= P4_4; //
  // P4_6
  P4DIR |= P4_5; //
  // P4_7
  P4DIR |= J3_29; //
}

//=========================================================================== 
// Description: Initializes all pins on port 5. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port5(void) {
  //Configure Port 5
  P5SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P5SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P5DIR = PORT_5_DIR_DEFAULT; // Set P1 direction to input
  
  // P5_0
  P5SEL0 |= UCB1SIMO; // 
  P5SEL1 &= ~UCB1SIMO; // 
  // P5_1
  P5SEL0 |= UCB1SOMI; // 
  P5SEL1 &= ~UCB1SOMI; // 
  // P5_2
  P5SEL0 |= UCB1SCK; // 
  P5SEL1 &= ~UCB1SCK; // 
  // P5_3
  P5OUT |= RESET_LCD; // 
  P5DIR |= RESET_LCD; //
  // P5_4
  P5OUT &= ~P5_4; // 
  P5DIR &= ~P5_4; //
  // P5_5
  P5DIR &= ~BUTTON2; //
  P5OUT |= BUTTON2; // 
  P5REN |= BUTTON2; // 
  // Button 2 Interrupt
  P5IES |= BUTTON2; // Hi/Lo edge interrupt
  P5IFG &= ~BUTTON2; // IFG cleared
  P5IE |= BUTTON2; // interrupt enabled
  // P5_6
  P5DIR &= ~BUTTON1; //
  P5OUT |= BUTTON1; // 
  P5REN |= BUTTON1; // 
  // Button 1 Interrupt
  P5IES |= BUTTON1; // Hi/Lo edge interrupt
  P5IFG &= ~BUTTON1; // IFG cleared
  P5IE |= BUTTON1; // interrupt enabled
  // P5_7
  P5DIR |= LCD_BACKLITE; //
  P5OUT |= LCD_BACKLITE; //
}

//=========================================================================== 
// Description: Initializes all pins on port 6. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port6(void) {
  //Configure Port 6
  P6SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P6SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P6DIR = PORT_6_DIR_DEFAULT; // Set P1 direction to input
  
  // P6_0
  P6SEL0 |= UCA3TXD; // 
  P6SEL1 &= ~UCA3TXD; // 
  // P6_1
  P6SEL0 |= UCA3RXD; // 
  P6SEL1 &= ~UCA3RXD; // 
  // P6_2
  P6DIR |= J1_5; // 
  // P6_3
  P6DIR |= MAG_INT; // 
  // P6_4
  P6DIR |= P6_4; // 
  // P6_5
  P6DIR |= P6_5; // 
  // P6_6
  P6DIR |= P6_6; // 
  // P6_7
  P6DIR |= P6_7; // 
}

//=========================================================================== 
// Description: Initializes all pins on port 7. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port7(void) {
  //Configure Port 7
  P7SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P7SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P7DIR = PORT_7_DIR_DEFAULT; // Set P1 direction to input
  
  // P7_0
  P7DIR |= I2CSDA; // 
  // P7_1
  P7DIR |= I2CSCL; // 
  // P7_2
  P7DIR |= SD_DETECT; // 
  // P7_3
  P7DIR |= J4_36; // 
  // P7_4
  P7DIR |= P7_4; // 
  // P7_5
  P7DIR |= P7_5; // 
  // P7_6
  P7DIR |= P7_6; // 
  // P7_7
  P7DIR |= P7_7; // 
}

//=========================================================================== 
// Description: Initializes all pins on port 8. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Port8(void) {
  //Configure Port 8
  P8SEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  P8SEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  P8DIR = PORT_8_DIR_DEFAULT; // Set P1 direction to input
  
  // P8_0
  P8DIR |= IR_LED; // 
  P8OUT &= ~IR_LED; // 
  // P8_1
  P8DIR |= OPT_INT; // 
  // P8_2
  P8DIR |= TMP_INT; // 
  // P8_3
  P8DIR |= INT2; // 
}

//=========================================================================== 
// Description: Initializes all pins on port J. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_PortJ(void) {
  //Configure Port J
  PJSEL0 = SEL0_GPIO_DEFAULT; // GP I/O
  PJSEL1 = SEL1_GPIO_DEFAULT; // GP I/O
  PJDIR = PORT_J_DIR_DEFAULT;// Set P1 direction to input
  
  // PJ_0
  // PJDIR |= PJ_0; // 
  // PJ_1
  // PJDIR |= PJ_1; // 
  // PJ_2
  // PJDIR |= PJ_2; // 
  // PJ_3
  // PJDIR |= PJ_3; // 
  // PJ_4
  PJSEL0 |= LFXIN; // 
  PJSEL1 &= ~LFXIN; // 
  PJDIR |= LFXIN; // 
  // PJ_5
  PJSEL0 |= LFXOUT; // 
  PJSEL1 &= ~LFXOUT; // 
  PJDIR |= LFXOUT; // 
  // PJ_6
  PJSEL0 &= ~HFXIN; // 
  PJSEL1 &= ~HFXIN; // 
  PJDIR |= HFXIN; // 
  // PJ_7
  PJSEL0 &= ~HFXOUT; // 
  PJSEL1 &= ~HFXOUT; // 
  PJDIR |= HFXOUT; // 
}