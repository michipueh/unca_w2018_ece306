// Timer calculation
// INTERVAL = Clock / Input Divider [ID] / Input Divider Expansion [T?xEX0] / (1 / Desired Time)
// Clock = 8,000,000 [MCLK, ACLK, SMCLK]
// Input Divider [ID] = 1,2,4,8
// Input Divider Expansion [T?xEX0] = 1,2,3,4,5,6,7,8 
// Desired time between interrupts: 
// 50ms = 0.05s
// => 8,000,000 / 2 / 8 / (1 / 0.05) = 25000
#define TA0CCR0_INTERVAL                (25000)

// 10ms = 0.01s
// => 8,000,000 / 2 / 8 / (1 / 0.01) = 5000
#define TA0CCR1_INTERVAL                (5000)
// 10ms = 0.01s
// => 8,000,000 / 2 / 8 / (1 / 0.01) = 5000
#define TA0CCR2_INTERVAL                (5000)

#define TA0CCR1_BUTTON_DEBOUNCE         (100)

#define ISR_TIMERS_CASE_NO_INTERRUPT    (0)
#define ISR_TIMERS_CASE_CCR1            (2)
#define ISR_TIMERS_CASE_CCR2            (4)
#define ISR_TIMERS_CASE_OVERFLOW        (14)
#define ISR_TIMERS_RANGE_BOUND          (14)

// Timers
void Init_Timers(void);
void Init_Timer_A0(void);
void Init_Timer_A1(void);
void Init_Timer_A2(void);
void Init_Timer_A3(void);
void Init_Timer_B0(void);
void Init_Timer_B1(void);
void Init_Timer_B2(void);