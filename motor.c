//=========================================================================== 
//
// Description: This file contains implementation of the motor controls
//
// Michael Puehringer
// Oct 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "motor.h"
#include "macros.h"
#include "msp430.h"

//=========================================================================== 
// Description: Checks if the left motor is turning forward. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: 1 if true, 0 if false. 
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
int is_left_motor_forward(void) {
  return (LEFT_FORWARD_SPEED != WHEEL_OFF);
}
//=========================================================================== 
// Description: Checks if the left motor is turning reverse. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: 1 if true, 0 if false. 
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
int is_left_motor_reverse(void) {
  return (LEFT_REVERSE_SPEED != WHEEL_OFF);
}

//=========================================================================== 
// Description: Turns the left motor completely off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void left_motor_off(void) {
  left_motor_reverse_off();
  left_motor_forward_off();
}

//=========================================================================== 
// Description: Turns the left motor reverse off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void left_motor_reverse_off(void) {
  if(is_left_motor_reverse()) {
    LEFT_REVERSE_SPEED = WHEEL_OFF;
    __delay_cycles(ONE_MS_CYCLES);
  }
}

//=========================================================================== 
// Description: Turns the left motor forward off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void left_motor_forward_off(void) {
  if(is_left_motor_forward()) {
    LEFT_FORWARD_SPEED = WHEEL_OFF;
    __delay_cycles(ONE_MS_CYCLES);
  }
}

//=========================================================================== 
// Description: Turns the left motor reverse off and sets forward on
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void left_motor_forward(void) {
  left_motor_reverse_off();
  LEFT_FORWARD_SPEED = LEFT_WHEEL_ON;
}

//=========================================================================== 
// Description: Turns the left motor reverse off and sets forward to a specific speed
// 
// Passed: 
//   float speed .. speed between 0 and 1 (0% to 100%)
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void left_motor_forward_speed(float speed) {
  left_motor_reverse_off();
  LEFT_FORWARD_SPEED = (int) (LEFT_WHEEL_ON * speed);
}

//=========================================================================== 
// Description: Turns the left motor completely off and sets reverse on
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void left_motor_reverse(void) {
left_motor_forward_off();
  LEFT_REVERSE_SPEED = LEFT_WHEEL_ON;
}

//=========================================================================== 
// Description: Turns the left motor forward off and sets reverse to a specific speed
// 
// Passed: 
//   float speed .. speed between 0 and 1 (0% to 100%)
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void left_motor_reverse_speed(float speed) {
  left_motor_forward_off();
  LEFT_REVERSE_SPEED = (int) (LEFT_WHEEL_ON * speed);
}

//=========================================================================== 
// Description: Checks if the right motor is turning forward. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: 1 if true, 0 if false. 
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
int is_right_motor_forward(void) {
  return (RIGHT_FORWARD_SPEED != WHEEL_OFF);
}

//=========================================================================== 
// Description: Checks if the right motor is turning reverse. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: 1 if true, 0 if false. 
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
int is_right_motor_reverse(void) {
  return (RIGHT_REVERSE_SPEED != WHEEL_OFF);
}

//=========================================================================== 
// Description: Turns the right motor completely off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void right_motor_off(void) { 
  right_motor_reverse_off();
  right_motor_forward_off();
}

//=========================================================================== 
// Description: Turns the right motor reverse off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void right_motor_reverse_off(void) { 
  if(is_right_motor_reverse()) {
    RIGHT_REVERSE_SPEED = WHEEL_OFF;
    __delay_cycles(ONE_MS_CYCLES);
  }
}

//=========================================================================== 
// Description: Turns the right motor forward off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void right_motor_forward_off(void) { 
  if(is_right_motor_forward()) {
    RIGHT_FORWARD_SPEED = WHEEL_OFF;
    __delay_cycles(ONE_MS_CYCLES);
  }

}

//=========================================================================== 
// Description: Turns the right motor completely off and sets forward on
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void right_motor_forward(void) {
  right_motor_forward_speed(ONE);
}

//=========================================================================== 
// Description: Turns the right motor reverse off and sets forward to a specific speed
// 
// Passed: 
//   float speed .. speed between 0 and 1 (0% to 100%)
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void right_motor_forward_speed(float speed) {
  right_motor_reverse_off();
  RIGHT_FORWARD_SPEED = (int) (RIGHT_WHEEL_ON * speed);
}

//=========================================================================== 
// Description: Turns the right motor completely off and sets reverse on
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void right_motor_reverse(void) {
  right_motor_reverse_speed(ONE);
}

//=========================================================================== 
// Description: Turns the right motor forward off and sets reverse to a specific speed
// 
// Passed: 
//   float speed .. speed between 0 and 1 (0% to 100%)
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void right_motor_reverse_speed(float speed) {
  right_motor_forward_off();
  RIGHT_REVERSE_SPEED = (int) (RIGHT_WHEEL_ON * speed);
}

//=========================================================================== 
// Description: Turns the left and right motor completely off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void both_motors_off(void) {
  left_motor_off();
  right_motor_off();
}

//=========================================================================== 
// Description: Turns the left and right motor reverse off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void both_motors_reverse_off(void) {
  left_motor_reverse_off();
  right_motor_reverse_off();
}

//=========================================================================== 
// Description: Turns the left and right motor forward off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void both_motors_forward_off(void) {
  left_motor_forward_off();
  right_motor_forward_off();
}

//=========================================================================== 
// Description: Turns the left and right motor completely off and sets forward on both
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void both_motors_forward(void) {
  both_motors_forward_speed(ONE);
}

//=========================================================================== 
// Description: Turns both motors reverse off and sets forward to a specific speed
// 
// Passed: 
//   float speed .. speed between 0 and 1 (0% to 100%)
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void both_motors_forward_speed(float speed) {
  both_motors_reverse_off();
  left_motor_forward_speed(speed);
  right_motor_forward_speed(speed);
}

//=========================================================================== 
// Description: Turns the left and right motor completely off and sets reverse on both
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void both_motors_reverse(void) {
  both_motors_reverse_speed(ONE);
}

//=========================================================================== 
// Description: Turns both motors forward off and sets reverse to a specific speed
// 
// Passed: 
//   float speed .. speed between 0 and 1 (0% to 100%)
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void both_motors_reverse_speed(float speed) {
  both_motors_forward_off();
  left_motor_reverse_speed(speed);
  right_motor_reverse_speed(speed);
}