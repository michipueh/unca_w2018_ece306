//=========================================================================== 
//
// Description: This file contains the initialization of the IOT
//
// Michael Puehringer
// Nov 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "iot.h"
#include "serial.h"
#include "macros.h"
#include "functions.h"

volatile char ip_found = FALSE;
volatile char ssid_found = FALSE;
volatile char tcp_server_started = FALSE;
volatile char ip[IP_STR_LENGTH];
volatile char ssid[SSID_STR_LENGTH];
volatile char tcp_connected[ONE];

const iot_profile_e iot_profile_unca = {
  IOT_CMD_BAUDRATE_115200,
  IOT_CMD_SET_WPA_SSID_UNCA,
  IOT_CMD_SET_WPA_PSK_UNCA,
  IOT_CMD_SAVE_PROFILE_1,
  IOT_CMD_STARTUP_PROFILE_1
};

const iot_profile_e iot_profile_priv = {
  IOT_CMD_BAUDRATE_115200,
  IOT_CMD_SET_WPA_SSID_PRIV,
  IOT_CMD_SET_WPA_PSK_PRIV,
  IOT_CMD_SAVE_PROFILE_2,
  IOT_CMD_STARTUP_PROFILE_2
};

//=========================================================================== 
// Description: Sends commands to the IOT module to configure 
//              to the given profile. 
// 
// Passed : 
//   iot_profile_e profile .. profile to be configured
// Locals: no variables declared 
// Returned: no values returned 
// Globals: 
//   tcp_server_started
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void iot_profile_set(const iot_profile_e profile) {
  iot_reset_display_strings();
  tcp_server_started = FALSE;
  
  uart_write_string(UART3, IOT_CMD_FACTORY_RESET);
  delay_ms(FIVE);
  uart_write_string(UART3, profile.baudrate);
  delay_ms(FIVE);
  uart_write_string(UART3, IOT_CMD_L2_L3_STOP);
  delay_ms(FIVE);
  uart_write_string(UART3, IOT_CMD_LOGLVL_2);
  delay_ms(FIVE);
  uart_write_string(UART3, IOT_CMD_DHCP_START);
  delay_ms(FIVE);
  uart_write_string(UART3, IOT_CMD_WM_MODE_STATION);
  delay_ms(FIVE);
  uart_write_string(UART3, profile.ssid);
  delay_ms(FIVE);
  uart_write_string(UART3, profile.psk);
  delay_ms(FIVE);
  uart_write_string(UART3, IOT_CMD_L2_L3_START);
  delay_ms(FIVE);
  uart_write_string(UART3, profile.save_profile);
  delay_ms(FIVE);
  uart_write_string(UART3, profile.startup_profile);
  delay_ms(FIVE);
  uart_write_string(UART3, IOT_CMD_HARD_RESET);
}

//=========================================================================== 
// Description: Sends the TCP server start command to the IOT module. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// Globals: no variables used
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void iot_start_tcp_server(void) {
  uart_write_string(UART3, IOT_CMD_TCP_START);
}

//=========================================================================== 
// Description: Sends the NETSTAT command to the IOT module. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// Globals: no variables used
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void iot_network_status(void) {
  iot_reset_display_strings();
  uart_write_string(UART3, IOT_CMD_STATS);
}

//=========================================================================== 
// Description: Resets the ip and ssid strings used to display. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// Globals: 
//   ip_found
//   ssid_found
//   ip
//   ssid
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void iot_reset_display_strings(void) {
  ip_found = FALSE;
  ssid_found = FALSE;
  ip[ZERO] = STR_END;
  ssid[ZERO] = STR_END;
}