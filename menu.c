//=========================================================================== 
//
// Description: This file contains the implementation of the menu
//
// Michael Puehringer
// Oct 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include <string.h>
#include "menu.h"
#include "emitter.h"
#include "macros.h"
#include "adc.h"
#include "serial.h"
#include "line.h"
#include "iot.h"
#include "functions.h"

// variables from LCD.r43
extern char display_line[DISPLAY_LINES][DISPLAY_LINE_LENGTH];

// variables from ports_interrupt.c
extern volatile char is_bt1_clicked;
extern volatile char is_bt2_clicked;

// variables from timer_interrupts.c
extern volatile int timer0_A0_CCR0_counter;
extern volatile unsigned long time_in_s;

// variables from adc_interrupts.c
extern volatile int ADC_Thumb;
extern volatile int ADC_Right_Detector;
extern volatile int ADC_Left_Detector;

// variables from serial_interrupts.c
extern volatile char ip[IP_STR_LENGTH];
extern volatile char ssid[SSID_STR_LENGTH];
extern volatile char tcp_server_started;

// variables from command.c
extern char last_cmd_str[DISPLAY_LINE_LENGTH];
extern char has_received_cmd;
extern char has_ended_parcour;
extern unsigned long time_of_parcour;

// variables from serial.c
extern baud_rate_e baud_rate_UART0;
extern baud_rate_e baud_rate_UART3;

extern unsigned int left_white, right_white, 
left_black, right_black, 
left_ambient, right_ambient;

// variables from line.c
extern circling_e circleState;

// local variables
menu_e selected_menu = MAIN;
menu_e previous_menu = MAIN;
char selected_menu_item = ZERO;
char previous_menu_item = ZERO;
int menu_offset = ZERO;
char first_start = TRUE;

// GENERAL MENU FUNCTIONS
void menu_item_static_draw(drawable_menu_item_e* menu_item, int index, int selected) {
  strncpy(&display_line[index][ONE], menu_item->title, DISPLAY_LINE_LENGTH - ONE);
}

void menu_item_empty_draw(int item_index, int index, int selected) {
  strcpy(display_line[index], "          ");
}

void menu_item_empty_click(int item_index) {
  // Empty
}

// MAIN MENU
drawable_menu_item_e main_menu_items[] = {
  { "CONFIG", TRUE, FALSE }, 
  { "STATUS", TRUE, FALSE },
  { "PROJECT10", TRUE, FALSE },
  { "", FALSE, FALSE }, 
};

drawable_menu_e main_menu = { 
  main_menu_items, 
  sizeof main_menu_items / sizeof main_menu_items[ZERO],
  main_menu_draw,
  main_menu_click
};

// MAIN MENU FUNCTIONS
void main_menu_draw(int item_index, int index, int selected) {
  switch (item_index) {
  case ITEM_4: 
    //switch(circleState) {
    //case TURNED_OFF: 
    //  strcpy(display_line[index], "   START  ");
    //  break;
    //case SEARCH_FOR_LINE: 
    //  strcpy(display_line[index], "  SEARCH  ");
    //  break;
    //case LOST_LINE: 
    //  strcpy(display_line[index], "   LOST   ");
    //  break;
    //case ON_LINE: 
    //  strcpy(display_line[index], " ON  LINE ");
    //  break;
    //case RIGHT_OFF_LINE: 
    //  strcpy(display_line[index], " DRIFT  R ");
    //  break;
    //case LEFT_OFF_LINE: 
    //  strcpy(display_line[index], " DRIFT  L ");
    //  break;
    //}
    if(tcp_server_started) {
      strcpy(display_line[index], " TCP ON   ");
    } else {
      strcpy(display_line[index], " TCP OFF  ");
    }
    break;   
  }
}

void main_menu_click(int item_index) {
  switch (item_index) {
  case ITEM_1: 
    selected_menu = CONFIG;
    break;
  case ITEM_2:   
    selected_menu = STATUS;
    break; 
  case ITEM_3:   
    selected_menu = PROJECT_10;
    break; 
  case ITEM_4: 
    if(circleState == TURNED_OFF) {
      circleState = SEARCH_FOR_LINE;
    } else {
      circleState = TURNED_OFF;
    }
    break;   
  }
}

// CONFIG MENU
const drawable_menu_item_e config_menu_items[] = {
  { "LINE CALI", TRUE, FALSE },
  { "UART CONF", TRUE, FALSE },
  { "IOT  CONF", TRUE, FALSE },
};

const drawable_menu_e config_menu = { 
  config_menu_items, 
  sizeof config_menu_items / sizeof config_menu_items[ZERO], 
  menu_item_empty_draw,
  config_menu_click
};

// CONFIG MENU FUNCTIONS
void config_menu_click(int item_index) {
  switch (item_index) {
  case ITEM_1:   
    selected_menu = CALIBRATE_LINE;  
    break;
  case ITEM_2: 
    selected_menu = CONFIG_UART;
    break;   
  case ITEM_3: 
    selected_menu = CONFIG_IOT;
    break;   
  }
}

// STATUS MENU
const drawable_menu_item_e status_menu_items[] = {
  { (char*)ssid, TRUE, TRUE }, 
  { (char*)ip, TRUE, TRUE }, 
  { (char*)ip+NINE, TRUE, TRUE },
  { "", FALSE, TRUE },
};

const drawable_menu_e status_menu = { 
  status_menu_items, 
  sizeof status_menu_items / sizeof status_menu_items[ZERO], 
  status_menu_draw,
  menu_item_empty_click
};

// STATUS MENU FUNCTIONS
void status_menu_draw(int item_index, int index, int selected) {
  switch (item_index) {
  case ITEM_4: 
    int_to_string(time_in_s, &display_line[index][2]);
    break;
  }
}

// IOT CONFIG MENU
const drawable_menu_item_e iot_config_menu_items[] = {
  { "NETSTAT  ", TRUE, FALSE },
  { "START TCP", TRUE, FALSE },
  { "SET   PR1", TRUE, FALSE },
  { "SET   PR2", TRUE, FALSE }
};

const drawable_menu_e iot_config_menu = { 
  iot_config_menu_items, 
  sizeof iot_config_menu_items / sizeof iot_config_menu_items[ZERO], 
  menu_item_empty_draw,
  iot_conf_menu_click
};

extern const iot_profile_e iot_profile_unca;
extern const iot_profile_e iot_profile_priv;

// IOT CONFIG MENU FUNCTIONS
void iot_conf_menu_click(int item_index) {
  switch (item_index) {
  case ITEM_1: 
    iot_network_status();
    break;
  case ITEM_2: 
    iot_start_tcp_server();
    break;
  case ITEM_3: 
    iot_profile_set(iot_profile_unca);
    break;
  case ITEM_4: 
    iot_profile_set(iot_profile_priv);
    break;
  }
}

// UART CONFIG MENU
const drawable_menu_item_e uart_config_menu_items[] = {
  { "", FALSE, FALSE },
  { "", FALSE, FALSE },
  { "", FALSE, FALSE },
  { "", FALSE, FALSE }
};

const drawable_menu_e uart_config_menu = { 
  uart_config_menu_items, 
  sizeof uart_config_menu_items / sizeof uart_config_menu_items[ZERO], 
  uart_conf_menu_draw, 
  uart_conf_menu_click
};

// UART CONFIG MENU FUNCTIONS
void uart_conf_menu_draw(int item_index, int index, int selected) {
  switch (item_index) {
  case ITEM_1: 
    if(uart_is_receiving(UART0)) {
      strcpy(display_line[index], " REC0   ON");
    } else {
      strcpy(display_line[index], " REC0  OFF");
    }
    break;
  case ITEM_2: 
    if(uart_is_receiving(UART3)) {
      strcpy(display_line[index], " REC3   ON");
    } else {
      strcpy(display_line[index], " REC3  OFF");
    }
    break;
  case ITEM_3: 
    if(baud_rate_UART0 == RATE_115200) {
      strcpy(display_line[index], " UCA0 115K");
    } else if (baud_rate_UART0 == RATE_460800) {
      strcpy(display_line[index], " UCA0 460K");
    } else if (baud_rate_UART0 == RATE_9600) {
      strcpy(display_line[index], " UCA0 9.6K");
    }
    break;
  case ITEM_4: 
    if(baud_rate_UART3 == RATE_115200) {
      strcpy(display_line[index], " UCA3 115K");
    } else if (baud_rate_UART3 == RATE_460800) {
      strcpy(display_line[index], " UCA3 460K");
    } else if (baud_rate_UART3 == RATE_9600) {
      strcpy(display_line[index], " UCA3 9.6K");
    }
    break;
  }
  
}

void uart_conf_menu_click(int item_index) {
  switch (item_index) {
  case ITEM_1: 
    if(uart_is_receiving(UART0)) {
      uart_stop_receiving(UART0);
    } else {
      uart_start_receiving(UART0);
    }
    break;
  case ITEM_2: 
    if(uart_is_receiving(UART3)) {
      uart_stop_receiving(UART3);
    } else {
      uart_start_receiving(UART3);
    }
    
    break;
  case ITEM_3: 
    if(baud_rate_UART0 == RATE_460800) {
      uart_set_baudrate(UART0, RATE_115200);
    } else if (baud_rate_UART0 == RATE_115200) {
      uart_set_baudrate(UART0, RATE_9600);
    } else if (baud_rate_UART0 == RATE_9600) {
      uart_set_baudrate(UART0, RATE_460800);
    }
    break;   
  case ITEM_4: 
    if(baud_rate_UART3 == RATE_460800) {
      uart_set_baudrate(UART3, RATE_115200);
    } else if (baud_rate_UART3 == RATE_115200) {
      uart_set_baudrate(UART3, RATE_9600);
    } else if (baud_rate_UART3 == RATE_9600) {
      uart_set_baudrate(UART3, RATE_460800);
    }
    break;
  }
}

// LINE CALIB MENU
const drawable_menu_item_e line_calib_menu_items[] = {
  { "", FALSE, FALSE },
  { "", FALSE, FALSE },
  { "", FALSE, FALSE },
  { "", FALSE, TRUE }
};

const drawable_menu_e line_calib_menu = { 
  line_calib_menu_items, 
  sizeof line_calib_menu_items / sizeof line_calib_menu_items[ZERO], 
  line_calib_menu_draw,
  line_calib_menu_click,
};

// LINE CALIB MENU FUNCTIONS
void line_calib_menu_draw(int item_index, int index, int selected) {
  switch (item_index) {
  case ITEM_1: 
    strcpy(display_line[index], "  AMB     ");
    int_to_string(left_ambient, &display_line[index][6]);
    break;
  case ITEM_2: 
    strcpy(display_line[index], "  WHI     ");
    int_to_string(left_white, &display_line[index][6]);
    break;
  case ITEM_3: 
    strcpy(display_line[index], "  BLA     ");
    int_to_string(left_black, &display_line[index][6]);
    break;
  case ITEM_4: 
    strcpy(display_line[index], "    <>    ");
    if(selected_menu_item == ZERO) {
      int_to_string(ADC_Left_Detector, &display_line[index][ZERO]);
      int_to_string(right_ambient, &display_line[index][6]);
    } else if(selected_menu_item == ONE) {
      int_to_string(ADC_Left_Detector, &display_line[index][ZERO]);
      int_to_string(right_white, &display_line[index][6]);
    } else if (selected_menu_item == TWO) {
      int_to_string(ADC_Left_Detector, &display_line[index][ZERO]);
      int_to_string(right_black, &display_line[index][6]);
    }
    break;
  }
}

void line_calib_menu_click(int item_index) {
  switch (item_index) {
  case ITEM_1: 
    turn_emitter_off(); 
    delay_ms(FIVE);
    left_ambient = ADC_Left_Detector;
    right_ambient = ADC_Right_Detector;
    break;
  case ITEM_2: 
    turn_emitter_on();
    delay_ms(FIVE);
    left_white = ADC_Left_Detector;
    right_white = ADC_Right_Detector;
    break;
  case ITEM_3: 
    turn_emitter_on(); 
    delay_ms(FIVE);
    left_black = ADC_Left_Detector;
    right_black = ADC_Right_Detector;
    break;   
  }
}

// PROJECT 10 MENU
const drawable_menu_item_e project_10_menu_items[] = {
  { (char*)last_cmd_str, TRUE, TRUE }, 
  { "", FALSE, TRUE },
  { (char*)ip, TRUE, TRUE }, 
  { (char*)ip+NINE, TRUE, TRUE },
};

const drawable_menu_e project_10_menu = { 
  project_10_menu_items, 
  sizeof project_10_menu_items / sizeof project_10_menu_items[ZERO], 
  project_10_menu_draw,
  menu_item_empty_click
};

// PROJECT 10 MENU FUNCTIONS
void project_10_menu_draw(int item_index, int index, int selected) {
  unsigned long time;
  switch (item_index) {
  case ITEM_2: 
    time = time_of_parcour;
    if(has_received_cmd && !has_ended_parcour) {
      time = time_in_s;
    }
    int_to_string(time, &display_line[index][TWO]);
    break;
  }
}

// MAIN FUNCTIONS
void draw_menu(void) {
  // return to main menu
  if(is_bt2_clicked) {
    is_bt2_clicked = FALSE;
    selected_menu = MAIN;
  }
  
  // find menu to draw
  drawable_menu_e menu_to_draw = main_menu; // default
  if (selected_menu == CONFIG) {
    menu_to_draw = config_menu;
  //} else if (selected_menu == STATUS) {
  //  menu_to_draw = status_menu;
  } else if (selected_menu == PROJECT_10) {
    menu_to_draw = project_10_menu;
  } else if (selected_menu == CALIBRATE_LINE) {
    menu_to_draw = line_calib_menu;
  //} else if (selected_menu == CONFIG_UART) {
  //  menu_to_draw = uart_config_menu;
  } else if (selected_menu == CONFIG_IOT) {
    menu_to_draw = iot_config_menu;
  }
  
  // calculate offsets
  selected_menu_item = ADC_Thumb / ((ADC_THUMB_MAX + 50) / menu_to_draw.length);
  if(previous_menu != selected_menu) {
    if(selected_menu_item > DISPLAY_LINE_4) {
      menu_offset = selected_menu_item - DISPLAY_LINE_4;
    } else {
      menu_offset = ZERO;
    }
    previous_menu_item = selected_menu_item;
  } else if(previous_menu_item < selected_menu_item && selected_menu_item > DISPLAY_LINE_4) {
    menu_offset = selected_menu_item - DISPLAY_LINE_4;
    previous_menu_item = selected_menu_item;
  } else if(previous_menu_item > selected_menu_item && selected_menu_item - menu_offset <= ZERO) {
    menu_offset = selected_menu_item;
    previous_menu_item = selected_menu_item;
  }
  
  // if scrolling/empty options available, clear screen
  //if(menu_to_draw.length != DISPLAY_LINES) { 
  if(previous_menu != selected_menu || first_start) {
    first_start = FALSE;
    draw_empty_menu();
    Display_Update(ZERO,ZERO,ZERO,ZERO);
  }
  //}
  
  previous_menu = selected_menu;
  
  ClrDisplay();
  // for every item
  for(int i = ZERO; i < menu_to_draw.length; i++) {
    // check if it is visible
    if(is_menu_item_visible(i, menu_offset)) {
      drawable_menu_item_e menu_item = menu_to_draw.items[i];
      
      int new_index = i - menu_offset;
      // check if current menu item is clicked
      if(is_bt1_clicked && selected_menu_item == new_index) {
        is_bt1_clicked = FALSE;
        if(menu_item.static_click) {
          menu_item_empty_click(i);
        } else {
          menu_to_draw.onMenuClick(i);
        }
      }
      // and draw it 
      if(menu_item.static_draw) {
        menu_item_static_draw(&menu_item, new_index, new_index == selected_menu_item);
      } else {
        menu_to_draw.onMenuDraw(i, new_index, new_index == selected_menu_item);
      }
    }
  }
  draw_selected_line(selected_menu_item - menu_offset);
  Display_Update(ZERO,ZERO,ZERO,ZERO);
}

void draw_empty_menu() {
  for(int i = DISPLAY_LINE_1; i <= DISPLAY_LINE_4; i++) {
    strcpy(display_line[i], "          ");
  }
}

void draw_selected_line(char index) {
  display_line[index > DISPLAY_LINE_4 ? DISPLAY_LINE_4 : index][ZERO] = '>';
}

int is_menu_item_visible(char item, char offset) {
  return (item - offset >= ZERO && item - offset < DISPLAY_LINES);
}

void int_to_string(int value, char* string) {
  int i = ZERO;
  while(TRUE) {
    int mod = value % TEN;
    value = value / TEN;
    string[i] = (char) (mod + '0');
    i++;
    if(value < TEN && value == ZERO) {
      break;
    }
  }
  //string[i] = '\0';
  
  int start = ZERO;
  int end = i - ONE;
  while(start < end) {
    char temp = string[start];  
    string[start] = string[end]; 
    string[end] = temp; 
    start++; 
    end--; 
  }
}
