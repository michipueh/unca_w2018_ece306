typedef void (*cmd_func)(int[], int);

typedef struct {
  char* str;
  void (*cmd_func)(int[], int);
  char p_length;
} cmd_type_e;

typedef struct {
  void (*cmd_func)(int[], int);
  int p[2];
  char p_length;
} cmd_e;

void handle_cmd_fast();
void handle_cmd_slow();
void handle_cmd_confirm();
void handle_cmd_backlite();
void handle_cmd_start_line_detect();
void handle_cmd_stop_line_detect();
void handle_cmd_motor_forward(int p[], int length);
void handle_cmd_motor_reverse(int p[], int length);
void handle_cmd_motor_left(int p[], int length);
void handle_cmd_motor_right(int p[], int length);
void handle_cmd_motor_forward_p(int p[], int length);
void handle_cmd_motor_reverse_p(int p[], int length);
void handle_cmd_unknown(int p[], int length);

#define CMD_MIN_LENGTH          (CMD_START_LENGTH + CMD_PASSWORD_LENGTH + CMD_SEPARATOR_LENGTH + 1 + CMD_END_LENGTH)
#define CMD_PASSWORD            "1234"
#define CMD_PASSWORD_LENGTH     (4)
#define CMD_START               '$'
#define CMD_START_LENGTH        (1)
#define CMD_END                 '!'
#define CMD_END_LENGTH          (1)
#define CMD_SEPARATOR           ' '
#define CMD_SEPARATOR_LENGTH    (1)
#define CMD_BUFFER_SIZE         (10)
#define CMDS_SIZE               (12)
#define CMD_SEPARATOR_OFFSET    (5)
#define CMD_BASE_LENGTH         (CMD_START_LENGTH + CMD_PASSWORD_LENGTH + CMD_SEPARATOR_LENGTH)

cmd_e string_to_cmd(char* string, int length);
cmd_type_e get_cmd_type_by_name(const char* string, int length);
char has_cmd(void);
void add_cmd(cmd_e cmd);
void handle_next_cmd(void); 
int add_cmd_from_buffer(char* buffer, int start, int end, int buffer_size); 
int atoi(const char *s);