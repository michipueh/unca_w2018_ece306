//=========================================================================== 
//
// Description: This file contains the Main Routine - "While" Operating System
//
// Michael Puehringer
// Sep 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "msp430.h"
#include <string.h>
#include "functions.h"
#include "macros.h"
#include "led.h"
#include "motor.h"
#include "timers.h"
#include "emitter.h"
#include "menu.h"
#include "serial.h"
#include "adc.h"
#include "line.h"
#include "iot.h"
#include "ports.h"
#include "command.h"

// variables from ports_interrupt.c
extern volatile char is_bt1_clicked;
extern volatile char is_bt2_clicked;
// variables from serial_interrupts.c
extern volatile char uart0_rx[UART0_RX_LENGTH];
extern volatile char uart3_rx[UART3_RX_LENGTH];
extern unsigned int uart3_rx_rd;
extern volatile unsigned int uart3_rx_wr;
extern unsigned int uart0_rx_rd;
extern volatile unsigned int uart0_rx_wr;
extern volatile char uart3_has_cmd;
// variables from timers_interrupt.c
extern volatile unsigned long time_in_s;
extern volatile char time_changed;
// variables from command.c
extern unsigned long time_of_last_command;
// variables from line.c
extern circling_e circleState;

//=========================================================================== 
// Description: Main program loop. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// Globals: 
//   tcp_server_started
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void main(void) {
  // Disable the GPIO power-on default high-impedance mode to activate
  // previously configured port settings
  PM5CTL0 &= ~LOCKLPM5;
  Init_Ports();                        // Initialize Ports
  Init_Clocks();                       // Initialize Clock System
  Init_Conditions();                   // Initialize Variables and Initial Conditions
  Init_Timers();                       // Initialize Timers
  Init_LCD();                          // Initialize LCD
  Init_ADC();                          // Initialize ADC
  Init_Serial();                       // Initialize Serial

  // Initialization
  turn_emitter_off();
  both_motors_off();
  
  // Start receiving on both UARTs
  uart_start_receiving(UART0);
  uart_start_receiving(UART3);
  
  // Reset display
  draw_empty_menu();
    
  // Reset IOT-module and re-enable it after some time
  P3OUT &= ~IOT_RESET;
  delay_ms(TWO_HUNDRED);
  P3OUT |= IOT_RESET;
  
  while(ALWAYS) {    
    if(time_changed) {
      time_changed = FALSE;
      draw_menu();
    }  
    
    //uart0_rx_rd = add_cmd_from_buffer(uart0_rx, uart0_rx_rd, uart0_rx_wr, UART0_RX_LENGTH);
    if(uart3_has_cmd) {
      uart3_has_cmd = FALSE;
      uart3_rx_rd = add_cmd_from_buffer((char*)uart3_rx, uart3_rx_rd, uart3_rx_wr, UART3_RX_LENGTH);
    }
    
    handle_next_cmd();
    
    // if no command was received for 2s, turn both motors off
    if(time_of_last_command + TWO < time_in_s && circleState == TURNED_OFF) {
      both_motors_off();
    }
    
    process_line();
  }
}

//=========================================================================== 
// Description: Waits for a given amount of milliseconds. 
// 
// Passed: 
//   long ms .. milliseconds to be waited for
// Locals: no variables declared 
// Returned: no values returned 
// Globals: no variables used
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void delay_ms(long ms) {
  while(ms > ZERO) {
    __delay_cycles(TEN_MS_CYCLES);
    ms -= TEN;
  }
}