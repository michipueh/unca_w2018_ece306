char is_emitter_on(void);
void turn_emitter_off(void);
void turn_emitter_on(void);
void toggle_emitter(void);