//=========================================================================== 
//
// Description: This file contains the initialization for the program 
//
// Michael Puehringer
// Sep 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "macros.h"
#include "functions.h"

extern char display_line[DISPLAY_LINES][DISPLAY_LINE_LENGTH];
extern char *display[DISPLAY_LINES];
extern volatile unsigned char update_display;
extern volatile unsigned int update_display_count;

//=========================================================================== 
// Description: Initializes all the conditions of the program. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// Globals: 
//   display_line
//   display
//   update_display
//   update_display_count
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_Conditions(void){
  for(int i=ZERO;i<DISPLAY_LINE_LENGTH;i++){
    display_line[DISPLAY_LINE_1][i] = RESET_STATE;
    display_line[DISPLAY_LINE_2][i] = RESET_STATE;
    display_line[DISPLAY_LINE_3][i] = RESET_STATE;
    display_line[DISPLAY_LINE_4][i] = RESET_STATE;
  }
  
  display[DISPLAY_LINE_1] = &display_line[DISPLAY_LINE_1][ZERO];
  display[DISPLAY_LINE_2] = &display_line[DISPLAY_LINE_2][ZERO];
  display[DISPLAY_LINE_3] = &display_line[DISPLAY_LINE_3][ZERO];
  display[DISPLAY_LINE_4] = &display_line[DISPLAY_LINE_4][ZERO];
  update_display = ZERO;
  update_display_count = ZERO;
  //pwm_state = PORTS;
  // Interrupts are disabled by default, enable them.
  enable_interrupts();
}