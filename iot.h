#define IOT_TCP_PORT                "8080"
#define IOT_TCP_MAX_CONNECTIONS     "1"
#define IOT_CMD_TCP_START           "AT+NSTCP=" IOT_TCP_PORT "," IOT_TCP_MAX_CONNECTIONS
#define IOT_CMD_STATS               "AT+NSTAT=?"
#define IOT_CMD_AT                  "AT"
#define IOT_CMD_BAUDRATE_115200     "ATB=115200,8,N,1"
#define IOT_CMD_SAVE_PROFILE_1      "AT&W0"
#define IOT_CMD_SAVE_PROFILE_2      "AT&W1"
#define IOT_CMD_STARTUP_PROFILE_1   "AT&Y0"
#define IOT_CMD_STARTUP_PROFILE_2   "AT&Y1"
#define IOT_CMD_FACTORY_RESET       "AT&F"
#define IOT_CMD_HARD_RESET          "AT+RESET=1"
#define IOT_CMD_MAC_ADDRESS         "AT+NMAC=?"
#define IOT_CMD_SET_RADIO_ON        "AT+WRXACTIVE=1"
#define IOT_CMD_NETWORK_STATUS      "AT+NSTAT=?"
#define IOT_CMD_LOGLVL_0            "AT+LOGLVL=0"
#define IOT_CMD_LOGLVL_1            "AT+LOGLVL=1"
#define IOT_CMD_LOGLVL_2            "AT+LOGLVL=2"
#define IOT_CMD_L2_L3_START         "AT+NCMAUTO=0,1,0"
#define IOT_CMD_L2_L3_STOP          "AT+NCMAUTO=0,0,0"
#define IOT_CMD_WM_MODE_STATION     "AT+WM=0"
#define IOT_CMD_DHCP_START          "AT+NDHCP=1"
#define IOT_CMD_DHCP_STOP           "AT+NDHCP=0"
#define IOT_CMD_SET_WPA_PSK_UNCA    "AT+WWPA=\"uncasheville!\""
#define IOT_CMD_SET_WPA_SSID_UNCA   "AT+WAUTO=0,\"UNCA_PSK\""
#define IOT_CMD_SET_WPA_PSK_PRIV    "AT+WWPA=\"12345678!\""
#define IOT_CMD_SET_WPA_SSID_PRIV   "AT+WAUTO=0,\"pueh\""

#define IP_STR_LENGTH        (16)
#define SSID_STR_LENGTH      (11)

typedef struct {
  const char* baudrate;
  const char* ssid;
  const char* psk;
  const char* save_profile;
  const char* startup_profile;
} iot_profile_e;

void iot_profile_set(const iot_profile_e profile);
void iot_start_tcp_server(void);
void iot_network_status(void);
void iot_reset_display_strings(void);