//=========================================================================== 
//
// Description: This file contains the Timer ISRs
//
// Michael Puehringer
// Oct 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//===========================================================================

#include "timers.h"
#include "msp430.h"
#include "macros.h"
#include "ports.h"

// counting interrupts of timer0_A0_CCR2
volatile int timer0_A0_CCR2_counter = ZERO;
// counting interrupts of timer0_A0_CCR0
volatile int timer0_A0_CCR0_counter = ZERO;
// counting interrupts of bt1 for timer0_A1_CCR1 
volatile unsigned int debounce_count_bt1 = ZERO;
// counting interrupts of bt2 for timer0_A1_CCR1 
volatile unsigned int debounce_count_bt2 = ZERO;

volatile char is_bt1_debouncing = FALSE;
volatile char is_bt2_debouncing = FALSE;

volatile unsigned long time_in_s = ZERO;
volatile unsigned long time_in_ms = ZERO;
volatile char time_changed = ZERO;

// updates the display in LCD.r43
extern volatile unsigned char update_display;

//=========================================================================== 
// Description: ISR for Timer0_A0
// 
// Passed : no variables passed 
// Locals: no variables declared
// Returned: no variables returned
// Globals: 
//   timer0_A0_CCR0_counter
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_A0_ISR(void) {
  timer0_A0_CCR0_counter++;
  
  if(timer0_A0_CCR0_counter == TEN) { 
    timer0_A0_CCR0_counter = ZERO;
  }
  
  TA0CCR0 += TA0CCR0_INTERVAL;
}

//=========================================================================== 
// Description: ISR for Timer0_A1
// 
// Passed : no variables passed 
// Locals: no variables declared
// Returned: no variables returned
// Globals: 
//   debounce_count_bt1
//   debounce_count_bt2
//   is_bt1_debouncing
//   is_bt2_debouncing
//   
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
#pragma vector = TIMER0_A1_VECTOR
__interrupt void Timer0_A1_ISR(void) {
  switch(__even_in_range(TA0IV, ISR_TIMERS_RANGE_BOUND)) {
    case ISR_TIMERS_CASE_NO_INTERRUPT: break; // No interrupt
    case ISR_TIMERS_CASE_CCR1: // CCR1 is used as button debounce
      if(is_bt1_debouncing) {
        debounce_count_bt1++;
      } 
      if(is_bt2_debouncing) {
        debounce_count_bt2++;
      }
      
      if(debounce_count_bt1 >= TA0CCR1_BUTTON_DEBOUNCE) {
        is_bt1_debouncing = FALSE;
        debounce_count_bt1 = ZERO;
        P5IFG &= ~BUTTON1; // clear button interrupt flag
        P5IE |= BUTTON1; // enable button interrupt
      }
      
      if(debounce_count_bt2 >= TA0CCR1_BUTTON_DEBOUNCE) {
        is_bt2_debouncing = FALSE;
        debounce_count_bt2 = ZERO;
        P5IFG &= ~BUTTON2; // clear button interrupt flag
        P5IE |= BUTTON2; // enable button interrupt
      }
      
      if(!is_bt1_debouncing && !is_bt2_debouncing) {
        // none are debouncing
        TA0CCTL1 &= ~CCIE; // CCR1 disable interrupt
        TA0CCR1 = ZERO; // reset CCR1 counter
        
        //TA0CCR0 = ZERO; // reset CCR0 counter
        //TA0CCTL0 |= CCIE; // CCR0 enable interrupt
      } else {
        //TA0CCTL0 &= ~CCIE; // CCR0 disable interrupt
        //TA0CCR0 = ZERO; // reset CCR0 counter

        TA0CCR1 += TA0CCR1_INTERVAL; 
      }
      break;
    case ISR_TIMERS_CASE_CCR2: // CCR2 is used as time giver
      timer0_A0_CCR2_counter++;
      
      if(timer0_A0_CCR2_counter % TWENTY == ZERO) {
        // 0ms, 200ms, 400ms, 600ms, 800ms
        // update the display every 200ms
        time_in_ms += TWO_HUNDRED;
        time_changed = TRUE;
        update_display = TRUE;
      } 
      if(timer0_A0_CCR2_counter == HUNDRED) { 
        // one second passed
        time_in_s++;
        timer0_A0_CCR2_counter = ZERO;
      }
      TA0CCR2 += TA0CCR2_INTERVAL;
      break;
    case ISR_TIMERS_CASE_OVERFLOW: // overflow
      break;
    default: break;
  }
}