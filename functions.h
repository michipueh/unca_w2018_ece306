//******************************************************************************
//
//  Description: This file contains the Function prototypes
//
//  Jim Carlson
//  Aug 2013
//  Built with IAR Embedded Workbench Version: V4.10A/W32 (5.40.1)
//******************************************************************************
// Functions

// Initialization
void Init_Conditions(void);

// Interrupts
void enable_interrupts(void);
__interrupt void Timer1_A0_ISR(void);
__interrupt void TIMER0_A1_ISR(void);
__interrupt void Timer0_B0_ISR(void);
__interrupt void TIMER0_B1_ISR(void);
__interrupt void switch_interrupt(void);
__interrupt void eUSCI_B0_ISR(void);
__interrupt void eUSCI_A0_ISR(void);
__interrupt void eUSCI_A3_ISR(void);

// Clocks
void Init_Clocks(void);

// LCD
void Display_Update(char p_L1, char p_L2, char p_L3, char p_L4);
void Init_LCD(void);

void ClrDisplay(void);
void ClrDisplay_Buffer_0(void);
void ClrDisplay_Buffer_1(void);
void ClrDisplay_Buffer_2(void);
void ClrDisplay_Buffer_3(void);
void lcd_BIG_mid(void);
void lcd_4line(void);


// Special Functions
void delay_ms(long ms);



