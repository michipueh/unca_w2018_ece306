#define ITEM_1    (0)
#define ITEM_2    (1)
#define ITEM_3    (2)
#define ITEM_4    (3)

typedef enum {
  MAIN = 0,
  CONFIG,
  STATUS,
  PROJECT_10,
  CONFIG_UART,
  CONFIG_LINE,
  CALIBRATE_LINE,
  CONFIG_IOT 
} menu_e;

typedef struct drawable_menu_item_e drawable_menu_item_e;
typedef struct drawable_menu_e drawable_menu_e;

typedef void (*onMenuDrawFunc)(int item_index, int index, int selected);
typedef void (*onMenuClickFunc)(int item_index);

struct drawable_menu_item_e {
  char* title;
  int static_draw;
  int static_click;
};

struct drawable_menu_e {
  const drawable_menu_item_e* items;
  char length;
  onMenuDrawFunc onMenuDraw;
  onMenuClickFunc onMenuClick;
};

void draw_menu(void);
void draw_empty_menu(void);
void draw_selected_line(char);

void int_to_string(int, char*);
int is_menu_item_visible(char item, char offset);

void menu_item_static_draw(drawable_menu_item_e* menu_item, int index, int selected);
void menu_item_empty_draw(int item_index, int index, int selected);
void menu_item_empty_click(int item_index);

void main_menu_draw(int item_index, int index, int selected);
void main_menu_click(int item_index);

void status_menu_draw(int item_index, int index, int selected);
void status_menu_click(int item_index);

void project_10_menu_draw(int item_index, int index, int selected);

void config_menu_draw(int item_index, int index, int selected);
void config_menu_click(int item_index);

void iot_conf_menu_draw(int item_index, int index, int selected);
void iot_conf_menu_click(int item_index);

void uart_conf_menu_draw(int item_index, int index, int selected);
void uart_conf_menu_click(int item_index);

void line_conf_menu_draw(int item_index, int index, int selected);
void line_conf_menu_click(int item_index);

void line_calib_menu_draw(int item_index, int index, int selected);
void line_calib_menu_click(int item_index);