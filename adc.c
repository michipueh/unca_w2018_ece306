//=========================================================================== 
//
//  Description: This file contains the ADC initialization
//
//  Michael Puehringer
//  Oct 2018
//  Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "msp430.h"
#include "macros.h"
#include "adc.h"

//=========================================================================== 
// Description: Initializes the ADC. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void Init_ADC(void) {
  ADC12CTL0 = RESET_STATE; // Clear ADC12CTL0
  ADC12CTL0 |= ADC12SHT0_2; // 16 ADC clocks
  ADC12CTL0 |= ADC12SHT1_2; // 16 ADC clocks
  ADC12CTL0 |= ADC12MSC; // The first rising edge of the SHI signal triggers the sampling timer
  ADC12CTL0 |= ADC12ON; // ADC ON - Core Enabled
  
  ADC12CTL1 = RESET_STATE; // Clear ADC12CTL1
  ADC12CTL1 |= ADC12PDIV_0; // Pre divide by 1
  ADC12CTL1 |= ADC12SHS_0; // sample-and-hold source ADC12SC
  ADC12CTL1 |= ADC12SHP; // sourced from the sampling timer
  
  ADC12CTL1 &= ~ADC12ISSH_0; // sample-input signal is not inverted 
  ADC12CTL1 |= ADC12DIV_0; // ADC12_B clock divider � Divide by 1.
  ADC12CTL1 |= ADC12SSEL0; // MODCLK
  ADC12CTL1 |= ADC12CONSEQ_3; // Repeat-sequence-of-channels
  
  ADC12CTL2 = RESET_STATE; // Clear ADC12CTL2
  ADC12CTL2 |= ADC12DIV_0; // Pre-divide by 1
  ADC12CTL2 |= ADC12RES_2; // 12 bit (14 clock cycle conversion time)
  ADC12CTL2 &= ~ADC12DF_0; // Binary unsigned
  
  ADC12CTL2 |= ADC12PWRMD_0; // Regular power mode where sample rate is not restricted
  //ADC12CTL2 &= ~ADC12SR; // supports up to approximately 200 ksps
  
  ADC12CTL3 = RESET_STATE;
  ADC12CTL3 |= ADC12ICH3MAP_0; // external pin is selected for ADC input channel A26
  ADC12CTL3 |= ADC12ICH2MAP_0; // external pin is selected for ADC input channel A27
  ADC12CTL3 |= ADC12ICH1MAP_0; // external pin is selected for ADC input channel A28
  ADC12CTL3 |= ADC12ICH0MAP_0; // external pin is selected for ADC input channel A29
  ADC12CTL3 |= ADC12TCMAP_1;  // ADC internal temperature sensor channel is
  // selected for ADC input channel A30
  ADC12CTL3 |= ADC12BATMAP_1; // ADC internal 1/2 x AVCC channel is selected for
  // ADC input channel A31
  ADC12CTL3 |= ADC12CSTARTADD_0; // ADC12MEM0 conversion start address
  
  ADC12MCTL0 = RESET_STATE;
  ADC12MCTL0 |= ADC12WINC_0; // Comparator window disabled
  ADC12MCTL0 |= ADC12DIF_0; // Single-ended mode enabled
  ADC12MCTL0 |= ADC12VRSEL_0; // VR+ = AVCC, VR- = AVSS
  ADC12MCTL0 |= ADC12INCH_2; // channel = A2
  ADC12MCTL1 = RESET_STATE;
  ADC12MCTL1 |= ADC12WINC_0; // Comparator window disabled
  ADC12MCTL1 |= ADC12DIF_0; // Single-ended mode enabled
  ADC12MCTL1 |= ADC12VRSEL_0; // VR+ = AVCC, VR- = AVSS
  ADC12MCTL1 |= ADC12INCH_4; // channel = A4 Right
  ADC12MCTL2 = RESET_STATE;
  ADC12MCTL2 |= ADC12WINC_0; // Comparator window disabled
  ADC12MCTL2 |= ADC12DIF_0; // Single-ended mode enabled
  ADC12MCTL2 |= ADC12VRSEL_0; // VR+ = AVCC, VR- = AVSS
  ADC12MCTL2 |= ADC12INCH_5; // channel = A5 Left
  ADC12MCTL2 |= ADC12EOS; // End of Sequence
  ADC12IER0 |= ADC12IE2; //Generate Interrupt for MEM2 ADC Data load
  // ADC12IER0 |= ADC12IE0; // Enable ADC conv complete interrupt
  ADC12CTL0 |= ADC12ENC; // Enable conversion
  ADC12CTL0 |= ADC12SC; // Start sampling
}