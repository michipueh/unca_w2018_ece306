#define UART0_RX_LENGTH      (64)
#define UART0_TX_LENGTH      (64)
#define UART3_RX_LENGTH      (64)
#define UART3_TX_LENGTH      (64)

// Struct for BAUD rates
typedef enum {
  RATE_115200, 
  RATE_460800,
  RATE_9600
} baud_rate_e;

// Struct for channel selection
typedef enum {
  UART0, 
  UART3,
} uart_channel_e;

// Struct for pattern matching state machine
typedef struct {
  // Pattern to be matched for
  char* pattern;
  // Buffer where result should be stored
  char* buffer;
  // Length of the pattern
  const char pattern_length;
  // Internal counter of how many characters of the pattern were matched already
  char pattern_counter;
  // Internal counter of how many characters where stored in the buffer already
  char buffer_counter;
  // Length after how many characters the filling of the buffer should be stopped
  const char end_length;
  // Stop character on which the filling of the buffer should be stopped
  const char end_character;
  // Internal indicator if the buffer is currently being filled
  char reading;
} pattern_e;

// 9,600 Baud Rate
// 1. Calculate N = fBRCLK / Baudrate
// N = SMCLK / 9,600 => 8,000,000 / 9,600 = 833.333333
// if N > 16 continue with step 3, otherwise with step 2
// 2. OS16 = 0, UCBRx = INT(N)
// continue with step 4
// 3. OS16 = 1
//      

// ***** Continuation from previous slide
// 9,600 Baud Rate
// 1. Calculate N = fBRCLK / Baudrate
// N = SMCLK / 9,600 => 8,000,000 / 9,600 = 833.3333333
// if N > 16 continue with step 3, otherwise with step 2
// 2. OS16 = 0, UCBRx = INT(N)
// continue with step 4
// 3. OS16 = 1,
// UCx = INT(N/16),
//     = INT(N/16) = 833.333/16 => 52
// UCFx = INT([(N/16) � INT(N/16)] � 16)
//       = ([833.333/16-INT(833.333/16)]*16)
//      = (52.08333333-52)*16
//      = 0.083*16 = 1
// 4. UCSx is found by looking up the fractional part of N (= N-INT(N)) in table Table 18-4
// Decimal of SMCLK / 8,000,000 / 9,600 = 833.3333333 => 0.333 yields 0x49 [Table]
// 5. If OS16 = 0 was chosen, a detailed error calculation is recommended to be performed
// For 9,600                                  TX error (%) RX error (%)
// BRCLK   Baudrate UCOS16 UCBRx UCFx UCSx neg   pos   neg  pos
// 8000000 9600     1      52    1    0x49 -0.08 0.04 -0.10 0.14
// For 460,800                                TX error (%) RX error (%)
// BRCLK   Baudrate UCOS16 UCBRx UCFx UCSx neg   pos  neg   pos
// 8000000 460800   1      1     1    0x4A -0.08 0.04 -0.10 0.14
// For 115,200                                TX error (%) RX error (%)
// BRCLK   Baudrate UCOS16 UCBRx UCFx UCSx neg   pos  neg   pos
// 8000000 115200   1      4     5    0x55 -0.08 0.04< -0.10 0.14
#define BRW_460800      (17)
#define BRW_115200      (4)
#define BRW_9600        (52)
#define MCTLW_460800    (0x4A00)
#define MCTLW_115200    (0x5551)
#define MCTLW_9600      (0x4911)

#define ISR_SERIAL_CASE_NO_INTERRUPT   (0)
#define ISR_SERIAL_CASE_RXIFG          (2)
#define ISR_SERIAL_CASE_TXIFG          (4)
#define ISR_SERIAL_RANGE_BOUND         (8)


// Serial
void Init_Serial(void);
void Init_Serial_UCA0(const baud_rate_e baud_rate);
void Init_Serial_UCA3(const baud_rate_e baud_rate);
void uart_write_char(const uart_channel_e channel, const char tx);
void uart_write_string(const uart_channel_e channel, const char* tx);
char uart_read_char(const uart_channel_e channel, char* rx, char blocking);
char uart_read_string(const uart_channel_e channel, char* buffer, unsigned int length, char blocking);
void uart_start_receiving(const uart_channel_e channel);
void uart_stop_receiving(const uart_channel_e channel);
int uart_is_receiving(const uart_channel_e channel);
void uart_set_baudrate(const uart_channel_e channel, const baud_rate_e baud_rate);
char match_pattern(pattern_e* p, char rx);