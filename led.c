//=========================================================================== 
//
// Description: This file contains the initialization for all LED pins 
//
// Michael Puehringer
// Sep 2018
// Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "macros.h"
#include "led.h"
#include "msp430.h"
#include "ports.h"

//=========================================================================== 
// Description: Turns on/off all the LEDs. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no values returned 
// 
// Author: Michael Puehringer 
// Date: Sept 2018 
//===========================================================================
void Init_LEDs(void){
// Turns on both LEDs
  P1OUT &= ~RED_LED;
  P1OUT &= ~GRN_LED;
}