//=========================================================================== 
//
//  Description: This file contains the Emitter implementation to 
//               query and turn on/off the emitter diode
//
//  Michael Puehringer
//  Oct 2018
//  Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include "emitter.h"
#include "msp430.h"
#include "ports.h"

//=========================================================================== 
// Description: Checks if the IR led is turned on. 
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: 1 if true, 0 if false. 
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
char is_emitter_on(void) {
  return (P8OUT & IR_LED);
}

//=========================================================================== 
// Description: Turns the IR led off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void turn_emitter_off(void) {
  P8OUT &= ~IR_LED;
}

//=========================================================================== 
// Description: Turns the IR led on
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void turn_emitter_on(void) {
  P8OUT |= IR_LED;
}

//=========================================================================== 
// Description: Toggles the IR led on or off
// 
// Passed : no variables passed 
// Locals: no variables declared 
// Returned: no variables returned
// 
// Author: Michael Puehringer 
// Date: Oct 2018 
//===========================================================================
void toggle_emitter(void) {
  P8OUT ^= IR_LED;
}