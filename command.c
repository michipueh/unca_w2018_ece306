//=========================================================================== 
//
//  Description: This file contains the Command implementation which are 
//               primarely received from the WiFi module
//
//  Michael Puehringer
//  Oct 2018
//  Built with IAR Embedded Workbench Version: V7.12.1 (7.12.1.987)
//=========================================================================== 

#include <string.h>
#include "command.h"
#include "serial.h"
#include "macros.h"
#include "msp430.h"
#include "motor.h"
#include "functions.h"
#include "ports.h"
#include "line.h"
#include "emitter.h"

// variables from line.c
extern circling_e circleState;

// variables from timer_interrupts.c
extern volatile unsigned long time_in_s;

// Commands to be parsed
cmd_type_e commands[CMDS_SIZE] = {
  //{ "F",  handle_cmd_fast, ZERO },
  //{ "S",  handle_cmd_slow, ZERO },
  { "STARTLINE",  handle_cmd_start_line_detect, ZERO },
  { "STOPLINE",  handle_cmd_stop_line_detect, ZERO },
  //{ "$",  handle_cmd_confirm, ZERO },
  { "LB", handle_cmd_backlite, ZERO },
  //{ "MF", handle_cmd_motor_forward, ONE },
  //{ "MR", handle_cmd_motor_reverse, ONE },
  { "ML", handle_cmd_motor_left, ONE },
  { "MR", handle_cmd_motor_right, ONE },
  { "MFP",handle_cmd_motor_forward_p, TWO },
  { "MRP",handle_cmd_motor_reverse_p, TWO },
};

// Unknown command for invalid commands
cmd_type_e unknown_command = { "UNKNOWN", handle_cmd_unknown, TWO };

// Ring buffer for commands (allowing sequential execution)
cmd_e cmd_buffer[CMD_BUFFER_SIZE];
char cmd_wr = ZERO;
char cmd_rd = ZERO;

// Time of last command to stop motors
unsigned long time_of_last_command;

char has_received_cmd = FALSE;
char has_ended_parcour = FALSE;
unsigned long time_of_parcour = ZERO;

// Temporary buffer for current parsed command
char cmd_str[UART3_RX_LENGTH];
char cmd_str_wr = ZERO;

// Buffer with string of last executed command 
char last_cmd_str[DISPLAY_LINE_LENGTH] = "Waiting";

//=========================================================================== 
// Description: Searches ring buffer for commands and adds them to the 
//              cmd ring buffer. 
// 
// Passed : 
//   char* buffer    .. buffer to be searched
//   int start       .. start index to start searching
//   int end         .. end index to stop searching
//   int buffer_size .. size of the passed buffer
// Locals: 
//   char command_found
//   char command_added
//   int last_command_end_index
//   int last_command_start_index
//   char rc
// Returned: The index of where to start searching the next time. 
//   if command is found (but end of buffer was reached), return start index of found command
//   if command was added, return end index of added command
//   else, return the end of the buffer
// Globals: 
//   cmd_str
//   cmd_str_wr
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
int add_cmd_from_buffer(char* buffer, int start, int end, int buffer_size) {
  char command_found = FALSE;
  char command_added = FALSE;
  int last_command_end_index = start;
  int last_command_start_index = start;
  cmd_str_wr = ZERO;
  // while there are characters left
  while(start != end) {
    // get current character
    char rc = buffer[start];
    // if it is the start character ..
    if(rc == CMD_START) {
      // .. set flags and save current index
      command_added = FALSE;
      command_found = TRUE;
      last_command_start_index = start;
    } 
    
    // if command is found .. 
    if(command_found == TRUE) {
      // if end of command is reached (or end of buffer)
      if(rc == CMD_END || cmd_str_wr == sizeof cmd_str - ONE) {
        // end command string
        cmd_str[cmd_str_wr++] = '\0';
        // reset the flags
        command_found = FALSE;
        command_added = TRUE;
        // clear start character of current command (to avoid multiple readings)
        buffer[last_command_start_index] = ' ';
        // set end of current command
        last_command_end_index = start + ONE;
        // add to cmd queue
        add_cmd(string_to_cmd((char*) cmd_str, cmd_str_wr));
        // reset the temporary buffer
        cmd_str_wr = ZERO;
      } else {
        // otherwise, add character to the buffer
        cmd_str[cmd_str_wr++] = rc;
      }
    }
    // increase counter
    start++;
    start %= buffer_size;
  }
  
  // TODO THIS MIGHT SKIP CURRENTLY UNFINISHED COMMANDS IN THE BUFFER
  //return end;
  // if command is found (but end was reached)
  if(command_found) {
    // start at the beginning of this command (next time)
    return last_command_start_index;
  } else if(command_added) {
    // start at the end + 1 of the added command (next time)
    return last_command_end_index;
  } else {
    // start at the end (next time), as no command was in the buffer
    return end;
  }
}

//=========================================================================== 
// Description: Parses a string to an command struct. 
// 
// Passed : 
//   char* string    .. command string to be parsed
//   int length      .. length of the command string
// Locals: 
//   cmd_e res                
//   const char start_char     
//   const char* password_str  
//   const char sep_char       
//   char* command_str         
// Returned: 
//   cmd_e .. the parsed command
// Globals: no variables used
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
cmd_e string_to_cmd(char* string, int length) {
  cmd_e res = { handle_cmd_unknown, { ZERO, ZERO }, ONE };
  
  if(length < CMD_MIN_LENGTH) {
    // not long enough
  } else {    
    // COMMAND FORMAT: 
    // $1234 CMD PARAM1 PARAM2!
    const char start_char =        string[ZERO];
    const char* password_str =     string + CMD_START_LENGTH;
    const char sep_char =          string[CMD_SEPARATOR_OFFSET];
    char* command_str  =           string + CMD_BASE_LENGTH;
    
    if(start_char != CMD_START) {
      // invalid starting char
      return res;
    } else if(sep_char != CMD_SEPARATOR) {
      // invalid separator
      return res;
    } else if(strncmp(password_str, CMD_PASSWORD, sizeof CMD_PASSWORD - ONE)) {
      // invalid password
      return res;
    } 
    
    char const sep[] = { CMD_SEPARATOR, STR_END };
    char i = ZERO;
    char first = TRUE;
    char *cmd_token = strtok(command_str, sep);
    while(cmd_token != NULL) {
      // handle token
      if(first) {
        first = FALSE;
        cmd_type_e cmd_type = get_cmd_type_by_name(cmd_token, strlen(cmd_token));
        res.p_length = cmd_type.p_length;
        res.cmd_func = cmd_type.cmd_func;
      } else if (i < res.p_length) {
        res.p[i] = atoi(cmd_token);
        i++;
      } else {
        break;
      }
      cmd_token = strtok(NULL, sep);
    }
    
    if(i != res.p_length) {
      // parameter length does not match
      res.cmd_func = handle_cmd_unknown;
    }
  }
  
  return res;
}

//=========================================================================== 
// Description: Returns the command type by its string name.  
// 
// Passed : 
//   char* string    .. command string to be searched for
//   int length      .. length of the command string
// Locals: 
//   cmd_type_e cmd  .. temporary command variable      
// Returned: 
//   cmd_type_e      .. the command type if found, otherwise the "UNKNOWN" command
// Globals: 
//   cmd_type_e commands[]
//   cmd_type_e unknown_command
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
cmd_type_e get_cmd_type_by_name(const char* string, int length) {
  for(int i = ZERO; i < CMDS_SIZE; i++) {
    cmd_type_e cmd = commands[i];
    if(strncmp(string, cmd.str, length) == ZERO) {
      return cmd;
    }
  }
  return unknown_command;
}

//=========================================================================== 
// Description: Returns true if a command is available, false otherwise.  
// 
// Passed: no parameters passed
// Locals: no variables defined
// Returned: 
//   char        .. 1 if command is available, 0 otherwise
// Globals: 
//   int cmd_wr
//   int cmd_rd
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
char has_cmd(void) {
  return cmd_wr != cmd_rd;
}

//=========================================================================== 
// Description: Adds a command to the command ringbuffer.  
// 
// Passed: 
//   cmd_e cmd   .. command to be added
// Locals: no variables defined
// Returned: no values returned
// Globals: 
//   void (*cmd_func)(int[], int) handle_cmd_unknown
//   cmd_e cmd_buffer[] 
//   int cmd_wr
// 
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void add_cmd(cmd_e cmd) {
  if(cmd.cmd_func != handle_cmd_unknown) {
    cmd_buffer[cmd_wr++] = cmd;
    cmd_wr %= CMD_BUFFER_SIZE;
  }
}

//=========================================================================== 
// Description: Executes the next command in the command ringbuffer.  
// 
// Passed: no parameters passed
// Locals: 
//   cmd_e cmd                              .. temporary command variable
// Returned: no values returned
// Globals: 
//   unsigned long time_in_s                .. time is reset when first command is handled
//   unsigned long time_of_last_command     .. time is updated when command is handled
//   char has_received_cmd                  .. indicator if command is first to be received
//   int cmd_rd                             .. updates the index if command is handled
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_next_cmd(void) {
  if(has_cmd()) {
    if(has_received_cmd == FALSE) {
      has_received_cmd = TRUE;
      time_in_s = ZERO;
    }
    time_of_last_command = time_in_s;
    cmd_e cmd = cmd_buffer[cmd_rd++];
    cmd_rd %= CMD_BUFFER_SIZE;
    cmd.cmd_func(cmd.p, cmd.p_length);
  }
}

//=========================================================================== 
// Description: Command to change the baud rate of UART3 to 115200.  
// 
// Passed: no parameters passed
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_fast(void) {
  uart_set_baudrate(UART3, RATE_115200);
  //uart_write_string(UART0, "115,200");
}

//=========================================================================== 
// Description: Command to change the baud rate of UART3 to 9600.  
// 
// Passed: no parameters passed
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_slow(void) {
  uart_set_baudrate(UART3, RATE_9600);
  //uart_write_string(UART0, "9,600");
}

//=========================================================================== 
// Description: Command to send "I'M HERE" to UART0.  
// 
// Passed: no parameters passed
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_confirm(void){
  uart_write_string(UART0, "I'M HERE");
}

//=========================================================================== 
// Description: Command to toggle the LCD backlite on and off.  
// 
// Passed: no parameters passed
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_backlite(void) {
  //uart_write_string(UART0, "TOGGLING BACKLITE");
  //strcpy(display_line[DISPLAY_LINE_3], " BACKLITE ");
  P5OUT ^= LCD_BACKLITE; // toggle the lcd backlite
}

//=========================================================================== 
// Description: Command to go forward for a passed amount of milliseconds.  
// 
// Passed: 
//   int p[]            .. parameter array -> p[0] is time in milliseconds
//   int length         .. length of parameter array
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_motor_forward(int p[], int length) {
  //uart_write_string(UART0, "FORWARD");
  //strcpy(display_line[DISPLAY_LINE_3], " FORWARD  ");
  strncpy(last_cmd_str, "FORWARD   ", DISPLAY_LINE_LENGTH);
  both_motors_forward();
  delay_ms(p[ZERO]);
  both_motors_off();
}

//=========================================================================== 
// Description: Command to go reverse for a passed amount of milliseconds.  
// 
// Passed: 
//   int p[]            .. parameter array -> p[0] is time in milliseconds
//   int length         .. length of parameter array
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_motor_reverse(int p[], int length) {
  //uart_write_string(UART0, "BACKWARD");
  //strcpy(display_line[DISPLAY_LINE_3], " BACKWARD ");
  strncpy(last_cmd_str, "BACKWARD  ", DISPLAY_LINE_LENGTH);
  both_motors_reverse();
  delay_ms(p[ZERO]);
  both_motors_off();
}

//=========================================================================== 
// Description: Command to turn left for a passed amount of milliseconds.  
// 
// Passed: 
//   int p[]            .. parameter array -> p[0] is time in milliseconds
//   int length         .. length of parameter array
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_motor_left(int p[], int length) {
  //uart_write_string(UART0, "LEFT");
  //strcpy(display_line[DISPLAY_LINE_3], "   LEFT   ");
  strncpy(last_cmd_str, "LEFT      ", DISPLAY_LINE_LENGTH);
  right_motor_forward();
  left_motor_off();
  delay_ms(p[ZERO]);
  both_motors_off();  
}

//=========================================================================== 
// Description: Command to turn right for a passed amount of milliseconds.  
// 
// Passed: 
//   int p[]            .. parameter array -> p[0] is time in milliseconds
//   int length         .. length of parameter array
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_motor_right(int p[], int length) {
  //uart_write_string(UART0, "RIGHT");
  //strcpy(display_line[DISPLAY_LINE_3], "  RIGHT   ");
  strncpy(last_cmd_str, "RIGHT     ", DISPLAY_LINE_LENGTH);
  left_motor_forward();
  right_motor_off();
  delay_ms(p[ZERO]);
  both_motors_off();
}

//=========================================================================== 
// Description: Command to set the forward speed to a passed value.  
// 
// Passed: 
//   int p[]            .. parameter array -> p[0] is the speed (between 0 and 100)
//   int length         .. length of parameter array
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_motor_forward_p(int p[], int length) {
  //uart_write_string(UART0, "FORWARD");
  //strcpy(display_line[DISPLAY_LINE_3], " FORWARDP ");
  strncpy(last_cmd_str, "FORWARDP  ", DISPLAY_LINE_LENGTH);
  if(p[ZERO] <= ZERO) {
    left_motor_off();
  } else if(p[ZERO] <= HUNDRED) {
    left_motor_forward_speed((float)p[ZERO]/HUNDRED);
  }
  if(p[ONE] <= ZERO) {
    right_motor_off();
  } else if(p[ONE] <= HUNDRED) {
    right_motor_forward_speed((float)p[ONE]/HUNDRED);
  }
}

//=========================================================================== 
// Description: Command to set the reverse speed to a passed value.  
// 
// Passed: 
//   int p[]            .. parameter array -> p[0] is the speed (between 0 and 100)
//   int length         .. length of parameter array
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_motor_reverse_p(int p[], int length) {
  //uart_write_string(UART0, "BACKWARD");
  //strcpy(display_line[DISPLAY_LINE_3], " BACKWARDP");
  strncpy(last_cmd_str, "BACKWARDP ", DISPLAY_LINE_LENGTH);
  if(p[ZERO] <= ZERO) {
    left_motor_off();
  } else if(p[ZERO] <= HUNDRED) {
    left_motor_reverse_speed((float)p[ZERO]/HUNDRED);
  }
  if(p[ONE] <= ZERO) {
    right_motor_off();
  } else if(p[ONE] <= HUNDRED) {
    right_motor_reverse_speed((float)p[ONE]/HUNDRED);
  }
}

//=========================================================================== 
// Description: Command to indicate an unknown command. 
// 
// Passed: 
//   int p[]            .. parameter array
//   int length         .. length of parameter array
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_unknown(int p[], int length) {
  //uart_write_string(UART0, "UNKNOWN COMMAND");
  //strcpy(display_line[DISPLAY_LINE_3], " UNKNOWN  ");
  strncpy(last_cmd_str, "UNKNOWN   ", DISPLAY_LINE_LENGTH);
}

//=========================================================================== 
// Description: Command to handle the start of the line detection.  
// 
// Passed: no parameters passed
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_start_line_detect(void) {
  strncpy(last_cmd_str, "LINE      ", DISPLAY_LINE_LENGTH);
  turn_emitter_on();
  //delay_ms(HUNDRED);
  //both_motors_forward();
  //delay_ms(TWO_THOUSAND);
  //right_motor_off();
  //delay_ms(THOUSAND);
  //both_motors_forward();
  //delay_ms(TWO_THOUSAND);
  //right_motor_off();
  //delay_ms(FIVE_HUNDRED);
  //both_motors_forward();
  //delay_ms(THOUSAND);
  //both_motors_off();
  //delay_ms(FIVE_HUNDRED);
  //left_motor_forward_speed(1.0f);
  //right_motor_forward_speed(0.4f);
  //delay_ms(FIVE_THOUSAND);
  both_motors_off();
  circleState = SEARCH_FOR_WHITE;
}

//=========================================================================== 
// Description: Command to handle the stop of the line detection.  
// 
// Passed: no parameters passed
// Locals: no variables declared
// Returned: no values returned
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
void handle_cmd_stop_line_detect(void) {
  strncpy(last_cmd_str, "END       ", DISPLAY_LINE_LENGTH);
  turn_emitter_off();
  circleState = TURNED_OFF;
  both_motors_off();
  right_motor_forward();
  delay_ms(EIGHT_HUNDRED);
  both_motors_forward();
  delay_ms(THREE_THOUSAND);
  both_motors_off();
  time_of_parcour = time_in_s;
  has_ended_parcour = TRUE;
}

//=========================================================================== 
// Description: Helper function to convert a string to an integer.  
// Code adapted from http://e2e.ti.com/support/microcontrollers/msp430/f/166/t/291574
// 
// Passed: 
//   const char* s          .. string to be converted
// Locals: 
//   int a                  .. accumulator value (result)
//   int i                  .. current index in the string
//   char c                 .. character at the current index
//   int d                  .. temporary int value of the current character
// Returned: 
//   int                    .. integer value of the passed string
// Globals: no variables used
//   
// Author: Michael Puehringer 
// Date: Nov 2018 
//===========================================================================
//
int atoi(const char *s) {
  int a = ZERO; // Accumulator
  int i = ZERO; // Array index
  char c = s[i]; // Current character
  
  i++; //Advance the index
  
  // Convert while not null terminator and is a numeric character
  while((c != STR_END) && (c >= ZERO_CHAR) && (c <= NINE_CHAR)) {
    int d = c - ZERO_CHAR; // Convert character to digit
    a = a * TEN;  // Make room for digit
    a = a + d; // Add digit to accumulator
    c = s[i]; // Get next character
    i++; //Advance the index
  }
  return a;
}